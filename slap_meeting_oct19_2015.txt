SLAP Meeting Oct 29 2014

In Attendance :
	Matt    Nick   Rod    Grayson  Flaco   Mia
	Ofelia  Zubir  Omran  Jose     Jared   Britannia

Agenda :
	- GROW shit
	- Koch Block
	- Plasma Campaign
	- Worker Appreciation Day
	- Announcements

GROW Shit : *ALL PEOPLE WHO WANT TO ATTEND THE GROW PLEASE VISIT THE RSVP LINK!*
	
	https://docs.google.com/forms/d/1ftBhvDOLfmQhHc3QGGJwfY2sgI2nVvh5HSAfi2m0lJg/viewform?c=0&w=1&usp=mail_form_link

	Saturday and Sunday Locations are set. It'll be in NSC 117 at 8AM. 

	Still working on Friday location and food hookups.

Koch Block : 

	FSU is currently in the process of approving John Thrasher
	as President of FSU. This action is mostly designed as a symbol of
	solidarity with FSU organizers and will consist of mic checks, flyers,
	and banners.

	We plan on having it Monday at 12:30PM in front of the Student Union.

	There'll be a banner making party this Saturday at Rod's place at 2PM

Plasma Campaign :

	Research Questions :
		- Who is the person we need to target?
		- What is their contract with Citibank?
		- Is this a big student issue?

Worker Appreciation Day : 

	A day where we set up a table/party for workers with food and drinks
	in order to show student appreciation for workers and to build connections.
	We plan on having it the Monday of Thanksgiving week (or was it the week
	before?). Zubir is head of this.

Announcements :

	SJP : On November 12th at 6PM the SJP at USF will be bringing two speakers
	from Palestine to speak about their daily experiences of being a student
	under occupation. It's gonna be a really interesting event, y'all should 
	come. We'll be going there right after the meeting next week.
