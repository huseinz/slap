var jQ = jQuery.noConflict();

var imagesDomain = '//d2fss5beqk4xh8.cloudfront.net';

jQ(document).ready(function () {
    preloadImages(imagesDomain+"/img/main_menu_drop_block_bottom.png", 
                  imagesDomain+"/img/main_menu_bg.png", 
                  imagesDomain+"/img/receive_email_input_bg.png");
});

function preloadImages(){
    var images = (typeof arguments[0] == 'object') ? arguments[0] : arguments;
    for (var i = 0; i < images.length; i++) {
        jQ("<img>").attr("src", images[i]);
    }
}