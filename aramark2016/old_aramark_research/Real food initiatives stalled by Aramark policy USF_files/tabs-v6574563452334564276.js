Document_Ready( function(){
    //main slider
	
	if(document.getElementById('main_page_slider')){
		var main_slider = new slider({
			slider_id: "main_page_slider",
			speed: 400,
			play:5000
		});
	}
    var tab1 = new tabs({
        tab_menu:"tab1",
        tabs_container:"tab_content1"
    });
    
    var reg_tab = new tabs({
        tab_menu:"popup_tab_menu",
        tabs_container:"reg_forms_container"
    });
	
	
	if($('state') && $('school'))
	{
		$('state').selectedIndex = 0;
		$('school').selectedIndex = 0;
	}
	window.onresize = function(){
		//console.log(document.body.clientWidth);
	};
		
});



function tabs(options){
    var self = this
    var active_tab = 0;
    self.tab_menu = document.getElementById(options.tab_menu);
    self.tabs_container = document.getElementById(options.tabs_container);
    if(!self.tab_menu || !self.tabs_container){
        return;
    }
    self.item_class = options.item_class || "item";
    self.tab_menu_item = self.tab_menu.children;
    self.item = [];
    for(var i = 0;i < self.tabs_container.children.length;i++){
        if(self.tabs_container.children[i].className == self.item_class){
            self.item.push(self.tabs_container.children[i]);
        }
    }

    for(var u = 0;u < self.item.length;u++){
        if(u != active_tab){
            self.item[u].style.display = "none";
        }
		else
		{
			self.item[u].style.display = "";
		}
    }
    self.tab_menu_item[active_tab].className = "active";
    function toggle(event){
		
		console.log('toggle');
        var target = event && event.target || event.srcElement;
        while(target.nodeName != "UL"){
            if(target.nodeName == 'LI'){
                if(target.className != "active"){
                    var new_active = 0;
                    self.tab_menu_item[active_tab].className = "";
                    target.className = "active";
                    for(var i = 0;i < self.tab_menu_item.length;i++){
                        if(self.tab_menu_item[i].className == "active"){
                            new_active = i;
                            break;
                        }
                    }
                    self.item[active_tab].style.display = "none";
                    self.item[new_active].style.display = "block";
                    active_tab = new_active;
                }
                break;
            }
            target = target.parentNode;
        }
        return false;
    }
    add_event(self.tab_menu,"click",toggle);
}
