
	#wrap_map{
	display:none;
}
.seo_channel{
	display:none;
}

#main_menu .left_menu li .drop_block{
    display: none;
    width: 134px;
    padding: 10px 5px 5px 10px;
    position: absolute;
    top: 35px;
    left: 0px;
    z-index: 60;
	border:0;
    background: url(/img/main_menu_drop_block_repeat.png) repeat-y left top;
}

#main_menu .left_menu li:first-child + li + li + li + li .drop_block{
	top: 35px;
	left: -53px;
}
.os_apple #main_menu .left_menu li:first-child + li + li + li + li .drop_block{
	top: 35px;
	left: -52px;
}
#main_menu .left_menu .addition_menu_border{
    height: 3px;
    position: relative;
    z-index: 60;
    left: 131px;
    top: -12px;
    background: url(/img/main_menu_drop_block_repeat_right.png) repeat-y left top;
}
#main_menu .left_menu li .drop_block .drop_block_angels{
	position: absolute;
	bottom: -10px;
	left:0;
	width: 100%;
	height: 10px;
	background: url(/img/main_menu_drop_block_bottom.png) no-repeat left top;
}
#main_menu .left_menu li .drop_block a{
    font-size: 12px;
    color: #235188;
    text-decoration: none;
	display: block;
	margin-bottom: 8px;
	line-height: 14px;
}
#uloop_just_for_fun{
width:607px;
}
#uloop_just_for_fun p{
padding-left:0;
padding-right:0;
}

.news_date {
margin: 5px 10px 10px 0;
color: #666666;
}
.breadcrumb {
margin: 12px 0 0 0;
}

//#main_menu .left_menu li:first-child, #main_menu .right_menu li:first-child{
//margin:0;
//}

//#main_menu .left_menu li:first-child a, #main_menu .right_menu li:first-child a{
//line-height: 16px;
//}

#main_menu .left_menu li.drop_bg{
	padding-right: 10px;
}

.head_lines{
padding:0;
}

.head_lines .u {
display:none;
}

.alert-success{
margin-bottom: 20px;
color: green;
font-size: 22px;
}
.alert-error{
margin-bottom: 20px;
color: red;
font-size: 22px;
}

.sortable{
	cursor:pointer;
}
.sorting_asc{
	padding-right:10px;
	background: url("/img/order_bottom.png") no-repeat right 3px;
}

.sorting_desc{
	padding-right:10px;
	background: url("/img/order_top.png") no-repeat right 3px;
}

.CampusPostingForm input[type=text], .CampusPostingForm input[type=date], .CampusPostingForm input[type=password]{
	width:auto;
}
.ui-datepicker-trigger{
background: none;
border: none;
padding: 0;
margin: 0;
margin-left: 6px;

position: relative;
top: 5px;
}

.preview_block{
	margin-bottom:15px;
}
.preview_block a{
	color:#235188;
	text-decoration:none;
	font-size:22px;
}
.preview_block a:hover{
	text-decoration:underline;
}

#uloop_just_for_fun img[style*="float:right"]{
	margin:5px 0 5px 10px;
}
#uloop_just_for_fun img[style*="float:left"]{
	margin:5px 10px 5px 0;
}

.wp-caption{
	margin:5px auto;
	display:table;
	float:none;
}

.wp-caption[style*="float:left"]{
margin:5px 10px 5px 0;
}
.wp-caption[style*="float:right"]{
margin:5px 0 5px 10px;
}

.embeddedContent{
margin:5px;
}
.embeddedContent[style*="float: left"]{
margin:5px 10px 5px 0;
}
.embeddedContent[style*="float: right"]{
margin:5px 0 5px 10px;
}
.embeddedContent[style*="float:left"]{
margin:5px 10px 5px 0;
}
.embeddedContent[style*="float:right"]{
margin:5px 0 5px 10px;
}

.random_article_link li{
height:auto;
}
.news_list{
margin-right:15px;
min-height: 282px;
}

.news_list .item{
min-height:63px;
padding: 0 0 6px 0;
margin: 0 0 5px 0;
}

.random_article_link li a{
	font-size:15px;
}

.right_links_item p a{
	font-size:15px;
}

#uloop_just_for_fun ol, #uloop_just_for_fun ol li
{
	list-style-type:decimal;
	padding-bottom:0;
}
#uloop_just_for_fun ul, #uloop_just_for_fun ul li
{
	list-style-type:disc;
}
#uloop_just_for_fun ol,#uloop_just_for_fun ul{
	padding-left:20px;
	margin-top:10px;
	margin-bottom:10px;
}
#uloop_just_for_fun img{
max-width: 600px;
height: auto;
}

#cke_article_body textarea
{
 max-width: 100% !important; 
 width: 100% !important; 
}
#cke_article_body{
	margin-top:5px;
}

#login_menu{
margin-right:15px;
}
.post_subtitle{
font-size:20px;
}

.inside_page h1{
margin-bottom:15px;
}

.article_tags{
	margin-bottom:0;
	margin-top:15px;
	width:100%;
}

.add-title{
	float	:right;
	
}

.add-title a{

}

#wrap_table{
	margin-top:5px;
}
#add_form{margin-bottom:5px;}
#sliderTabs ul li {
    text-align: center;
    list-style-type: none;
}

#sliderTabs ul {
    margin: 0px !important;
    padding: 0px !important;
}
.item .post_no_image {
width: 90px;
height: 60px;
margin: 0 12px 0 0;
float: left;
background: url("/img/no-image-small.jpg") no-repeat 50% 50%;
padding: 2px;
border: 1px solid rgb(204, 204, 204);
}

.by-no-image{
padding-left: 100px;
}
ul.main-menu li{
vertical-align:middle !important;
}
.admin-header{
	height:auto !important;
}
 #sliderTabs ol {
    padding: 0px !important;
}


#sliderTabs{
		width: 608px;
		height:408px;
		margin: 0 auto;
		text-align: center;
	}
	#sliderTabs ul li{
		text-align: center;
	}
	#sliderTabs .slides img{
		max-width: 604px;
		max-height: 404px; 
		width: auto;
		display: inline-block;
	}
	#sliderTabs ul li .imgWrap{
		display: table-cell;
		height: 408px;
		vertical-align: middle;
		text-align: center;
		width: 604px;
		background: #fff;
                position: relative;
	}
        #sliderTabs ul li .imgCaption{
            position: absolute;
            color: #666;
            width: 100%;
            z-index: 100;
            text-align: center;
            background-color: white;
            padding: 6px 0;
            bottom: 0;
        }
	.flex-viewport{
		background: none repeat scroll 0% 0% #FFF;
		border: 4px solid #FFF;
		heigh: 400px;
		-webkit-border-radius: 4px; 
		-moz-border-radius: 4px; 
		-o-border-radius: 4px; 
		border-radius: 4px; 
		-webkit-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-moz-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-o-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		box-shadow: 0 1px 4px rgba(0,0,0,.2); 
	}
	.flex-control-thumbs {

		display: inline-block;
		 width: auto !important;
		background: none repeat scroll 0% 0% #FFF;
		border: 4px solid #FFF;
		-webkit-border-radius: 4px; 
		-moz-border-radius: 4px; 
		-o-border-radius: 4px; 
		border-radius: 4px; 
		-webkit-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-moz-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-o-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		box-shadow: 0 1px 4px rgba(0,0,0,.2); 
	}
	.flex-control-thumbs img{
		width: 100%;
		min-height: 60px;
	}
	.flex-control-thumbs li {
		margin: 0px !important;
    float: left !important;
    width: 100px !important;

		height: 60px;
		overflow: hidden;
	}
	.loading2 {
		background: url('images/loader.gif') no-repeat center center;
	}
	
#logo img{
	max-width:960px;
}

.random_article_link li a.image_link_logo {
	display: block;
	width: 150px;
	height: 99px;
	overflow: hidden;
	margin: 0 0 0 0;
	vertical-align: middle;
	background-color: #f0f0f0;
	line-height:99px;
}

.random_article_link li a.image_link_logo img {
width:150px;
opacity:0.5;
}

.news_list .item .block_for_image_logo {
display: block;
float: left;
margin: 3px 10px 0 0;
width: 90px;
height: 60px;
line-height:60px;
overflow: hidden;
vertical-align: middle;
background-color: #f0f0f0;
}
	.news_list .item .block_for_image_logo img{
	width:90px;
	opacity:0.5;
}

.image_link_no_logo {
display: block;
width: 150px;
height: 99px;
overflow: hidden;
margin: 0 0 0 0;
line-height:99px;
vertical-align: middle;
background-color: #f0f0f0;
}

.image_link_no_logo img{
width:150px;
	opacity:0.5;
}

.CampusPostingForm input.post_short_input{
width:80px;
}

@media screen and (max-width: 650px) {
 .zergentity img {
  width: 100% !important;
  height: 100% !important;
 }
 .zergentity {
  width: 100px !important;
  height: 100px;
 }
 #zerglayout {
  width: 83% !important;
 }

 #uloop_just_for_fun {
  width: auto;
 }

 .flex-control-nav {
  width: auto;
 }
}

@media screen and (max-width: 1024px) {
 
 .top_banner {
  width: auto !important;
  overflow: hidden;
 }
 .top_banner img{
  width: 100%;
  height: 100%;
 }
 
 #logo img {
    width: 100%;
 }

}

.article_center .news_list h3{
	text-transform:uppercase;
}

	@media screen and (max-width: 580px) {
		#login_menu {
		margin: 9px 9px 0px;
		width: auto;
	 }
	#header .login_search {
		width: 235px;
		margin: 0px auto;
		float: none;
	}
}
@media screen and (max-width: 460px) {
 #logo {
  #width: 50%;
 }
 #logo img {
  width: 100%;
 }
}
.sliderWrap{
		margin: 0 auto;
		width: 608px;
		position: relative;
	}
	#sliderTabs{
		width: 608px;
		height: 408px;
		margin: 0 auto;
		text-align: center;
	}
	#sliderTabs ul li{
		text-align: center;
	}
	#sliderTabs .slides img{
		max-width: 604px;
		max-height: 404px; 
		width: auto;
		display: inline-block;
	}
	#sliderTabs ul li .imgWrap{
		display: table-cell;
		height: 408px;
		vertical-align: middle;
		text-align: center;
		width: 604px;
		background: #fff;
	}
	.flex-viewport{
		background: none repeat scroll 0% 0% #FFF;
		border: 4px solid #FFF;
		heigh: 400px;
		-webkit-border-radius: 4px; 
		-moz-border-radius: 4px; 
		-o-border-radius: 4px; 
		border-radius: 4px; 
		-webkit-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-moz-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-o-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		width: 600px;
		height: 400px;
	}
	.flex-control-thumbs {

		display: inline-block;
		width: auto;
		background: none repeat scroll 0% 0% #FFF;
		border: 4px solid #FFF;
		-webkit-border-radius: 4px; 
		-moz-border-radius: 4px; 
		-o-border-radius: 4px; 
		border-radius: 4px; 
		-webkit-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-moz-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		-o-box-shadow: 0 1px 4px rgba(0,0,0,.2); 
		box-shadow: 0 1px 4px rgba(0,0,0,.2); 
	}
	.flex-control-thumbs img{
		width: 100%;
		min-height: 60px;
	}
	.flex-control-thumbs li {
		margin: 0px;
		float: left;
		width: 100px;
		height: 60px;
		overflow: hidden;
	}
	.loading2 {
		background: url('images/loader.gif') no-repeat center center;
	}
	.flex-direction-nav {
		position: absolute;
		top: 0;
		left: 0;
		width: 608px;
		height: 408px;
	}
	.sliderNav {
    margin: 0px auto;
    text-align: center;
}
	
	@media screen and (max-width: 650px) {
		.sliderWrap {
			width: auto;
		}
		.flex-viewport{
			width: auto;
		}
		.flex-direction-nav{
			width: 100%;
		}
		#sliderTabs, .sliderNav{
			width: 95%;
		}
		
		.flex-direction-nav .flex-prev,
		.flex-direction-nav:hover .flex-prev {
			left: 20px;
			}
		.flex-direction-nav .flex-next,
		.flex-direction-nav:hover .flex-next {
			right: 20px;
			}
		/*.flex-control-thumbs li {
			width: 20%;
		}*/
		.flex-control-thumbs li {
			width: 112px;
		}
	}
	@media screen and (max-width: 500px) {
		#sliderTabs, .flex-direction-nav{
			height: 308px;
		}
		.flex-viewport{
			height: 300px;
		}
		#sliderTabs .slides img {
			max-width: 480px;
			max-height: 304px;
		}
		#sliderTabs ul li .imgWrap {
			height: 308px;
		}
		.flex-control-thumbs li {
			width: 108px;
		}
	}
	@media screen and (max-width: 360px) {
		.flex-control-thumbs li {
			width: 93px;
		}
		#sliderTabs .slides img {
			max-width: 300px;
		}
	}
	
#main_menu .left_menu li .drop_block .drop_block_angels {
    background: url('/img/main_menu_drop_block_bottom_gray.png') no-repeat scroll left top transparent;
}
#main_menu .left_menu li .drop_block {
    background: url('/img/main_menu_drop_block_repeat_gray.png') repeat-y scroll left top transparent;
}

.index_inside_page{
	margin-top: 6px;
}

.index_right_content .wrap_b_side:first-child{
padding-top:6px !important;
}

.slides, .flex-control-nav, .flex-direction-nav {
    margin: 0px !important;
    padding: 0px !important;
    list-style: none outside none !important;
	list-style-type: none !important;
	
}

#sliderTabs {
    margin-bottom: 5px !important;
}

#uloop_just_for_fun .flex-direction-nav li {
    list-style-type: none;
}

img{
 -webkit-backface-visibility: hidden;
 -webkit-transform: rotate(0);
 -moz-transform: rotate(0);
 transform: rotate(0);
}

.slider .subtitle
{
display: block;
font-size: 18px;
line-height:20px;
}

.post_classifieds_button
{
margin-left:15px;
}

.ArticleForm label{
margin-left:5px;
}
	#main_menu .right_menu li:first-child{ margin-right:15px; }
				#main_menu .right_menu li {
				margin: 0 0 0 19px;
				padding: 0 13px 0 0;
				}.orange_button_h_36,
.orange_button_h_44,
.orange_button_h_144,
.orange_button_h_145,
.go,
.go2,
.orange_save_h_22,
.email_submit {
background: rgb(191,210,85); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2JmZDI1NSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzhlYjkyYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUxJSIgc3RvcC1jb2xvcj0iIzcyYWEwMCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM5ZWNiMmQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(191,210,85,1) 0%, rgba(142,185,42,1) 50%, rgba(114,170,0,1) 51%, rgba(158,203,45,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(191,210,85,1)), color-stop(50%,rgba(142,185,42,1)), color-stop(51%,rgba(114,170,0,1)), color-stop(100%,rgba(158,203,45,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(191,210,85,1) 0%,rgba(142,185,42,1) 50%,rgba(114,170,0,1) 51%,rgba(158,203,45,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bfd255', endColorstr='#9ecb2d',GradientType=0 ); /* IE6-8 */
}