var jQ = jQuery.noConflict();


var iphone = false;

var results_container = false;
var search_timer = false;

var search_value = '';

var search_row_index = -1;

var imagesDomain = '//d2fss5beqk4xh8.cloudfront.net';

/*
jQ(document).ready(function(){
    Iphone.IphoneStart({navigation: true});
});

jQ('.popup_overlay, .loading').fadeIn(500);
            
            jQ(document).ready(function(){
                //window.onload(function(){
                jQ('.popup_overlay, .loading').fadeOut(500);
            });

*/


//SHOULD be rewritten for API mode
function Campus_OpenLink(page)
{
	location.href = page;
	return;
        var sessionParams = AJAXSessionParam;

	var unique = Math.round(Math.random()*1000);
	var BaseURL = AJAXBaseURL;
	if(page.indexOf('?')>=0)
            page = page + '&';
	else
            page = page + '?';
	var link =  page;
	if(page.indexOf('http')<0)
            link = BaseURL+'/'+link;
	location.href = page;
}

jQ(document).ready(function(){
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){
    
        iphone = true;
        
        Iphone.IphoneStart();

        document.addEventListener('click', function(event) {

                var noddy = event.target;

                // Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
                while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
                    noddy = noddy.parentNode;
                }

                if('href' in noddy && noddy.href.indexOf('http') !== -1 && noddy.host.indexOf('uloop.com') !==-1)
                {
                        event.preventDefault();
                        document.location.href = noddy.href;
                }

        },false);
        
}
});




(function( jQ ){
    
    var Iphone = function (options) {
        this.defaults = {
                            navigation: true,
                            preloader: true,
                            resize: true
                        };
        jQ.extend(true, this.defaults, options);
    };
    
    Iphone.prototype = {
        IphoneStart: function(options){
                var self = this;
                this.options = {};
                var options = this.options = jQ.extend(true, this.defaults, options);
            
                // Add Navigation
                if (options.navigation) {
                    self.navigation = self.navigation(options);
                }
                
                if (options.preloader) {
                    self.preloader = self.preloader(options);
                }
                
        },
        
        
        navigation: function (options) {
                
                jQ('#iphone-nav-left').on('click', function (e) {
                    e.preventDefault(); e.stopPropagation();
                    window.history.back();
                });
                
                
                jQ('#iphone-nav-refresh').on('click', function (e) {
                    e.preventDefault(); e.stopPropagation();
                    window.location.reload();
                });
                
                jQ('#iphone-nav').show();  
                
        },
        preloader: function (options){
            
            jQ('.popup_overlay, .loading').fadeIn();
            
            jQ(document).ready(function(){
                jQ('.popup_overlay, .loading').fadeOut(500);
            });
        }
       
    
    };
    
    window.Iphone = new Iphone();
    
})( jQuery );


var OnLoadStack = [];
function Document_Ready(func){
	OnLoadStack.push(func);
}

function Get_Input_Value(id){
	var elem = document.getElementById(id);
	
	if(elem.getAttribute('default-value') && elem.value == elem.getAttribute('default-value')){
		return '';
	}
	else{
		return elem.value;
	}
}

function Clear_Form_Error(id){
	if(document.getElementById(id+'-error'))
		document.getElementById(id+'-error').innerHTML='';
}

window.onload = function(){
	for (var key in OnLoadStack) {
		var func = OnLoadStack [key];
		func();
	}
	
}

function onlyNumber(event){
    //backspace, delete, tab и escape
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
             // Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
}


function getPageScroll(){
    var left = 0;
    var top = 0;
    if(window.pageYOffset || document.documentElement.scrollTop){
        top = window.pageYOffset || document.documentElement.scrollTop;
        left = window.pageXOffset || document.documentElement.scrollLeft;
    }else{
        var html = document.documentElement;
        var body = document.body;

        var scrollTop = html.scrollTop || body && body.scrollTop || 0;
        var scrollLeft = html.scrollLeft || body && body.scrollLeft || 0;
        top = scrollTop - html.clientTop;
        left = scrollLeft - html.clientLeft;
    }
    return {
        top:top,
        left:left
    };
} 

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function validationEmail(email, emailERROR)
{
     if(email.value.length===0 || CMPS_hasHTMLTags(email.value) || !isValidEmailAddress(email.value))
     {
        if(email.value.length===0)
            CMPS_SetErrorText_noneScroll(emailERROR, jQ(emailERROR).text());
        else if(CMPS_hasHTMLTags(email.value))
            CMPS_SetErrorText_noneScroll(emailERROR,' Please enter text only');
        else if(!isValidEmailAddress(email.value))
            CMPS_SetErrorText_noneScroll(emailERROR,' Please enter correct email');
        email.focus();
        email.onkeydown = function()
        {
            if(emailERROR) 
            {
                emailERROR.style.visibility = 'hidden';
                this.onkeydown=null;
            }
         }
         return false;
     }
     else return true;
}

function validationInput(input, str_error)
{
    if(input.value.length===0 || CMPS_hasHTMLTags(input.value))
    {
        if(input.value.length===0)
            CMPS_SetErrorText_noneScroll(str_error, jQ(str_error).text());
        else if(CMPS_hasHTMLTags(input.value))
            CMPS_SetErrorText_noneScroll(str_error,' Please enter text only');
        input.focus();
        input.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        return false;
    }
    else return true;
}

function validationShowError(input, str_error)
{
    
        
            CMPS_SetErrorText_noneScroll(str_error, jQ(str_error).text());
        
        input.focus();
        input.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        return false;
    
}

function validationPhone(phone1, phone2, phone3, str_error)
{
    if(phone1.value.length<3  || phone2.value.length<3 || phone3.value.length<4 || CMPS_hasHTMLTags(phone1.value) || CMPS_hasHTMLTags(phone2.value) || CMPS_hasHTMLTags(phone3.value))
    {
        if(phone1.value.length==0 || phone2.value.length==0 || phone3.value.length==0)
            CMPS_SetErrorText_noneScroll(str_error, jQ(str_error).text());
        else if(CMPS_hasHTMLTags(phone1.value) || CMPS_hasHTMLTags(phone2.value) || CMPS_hasHTMLTags(phone3.value))
            CMPS_SetErrorText_noneScroll(str_error,' Please enter text only');
        else if(phone1.value.length<3  || phone2.value.length<3 || phone3.value.length<4)
        {
            CMPS_SetErrorText_noneScroll(str_error,' Please enter correct Phone');
        }
        phone1.focus();
        phone1.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        phone2.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        phone3.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        return false;
    }
    else return true;
}
//open school select
function schoolSelect(){
    jQ('.main_school_select').slideDown(300);
    jQ('.link_choise_select').fadeOut(100);
    return false;
}
//close school select
function closeSchoolsSelect(){
    jQ('.main_school_select').slideUp(300);
    jQ('.link_choise_select').fadeIn(100);
    return false;
}

function validationPassws(passw, confirmPassw, str_error)
{
    if(passw.value!==confirmPassw.value)
    {
        //if(passw.value.length<=5) || passw.value.length<=5 || confirmPassw.value.length<=5
            //CMPS_SetErrorText_noneScroll(str_error,' Please enter more than 5 characters');
        if(passw.value!==confirmPassw.value)
            CMPS_SetErrorText_noneScroll(str_error,' Entered different passwords');
        passw.focus();
        passw.onkeydown = function()
        { 
            if(str_error) 
            {
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            } 
        }
        confirmPassw.onkeydown = function()
        { 
            if(str_error) 
            {  
                str_error.style.visibility = 'hidden';
                this.onkeydown=null;
            }
        }
        return false;
    }
    else return true;
}

function FaceBookLogin(noReload, fromPosring){
    var callbackFunction = false;
    if( !noReload )
        noReload = false;
    if( fromPosring )
        callbackFunction = true;
    
    var redirect = jQ('#globalLoginRedirect').val();
          
    var Width = ((jQ("body").width()-420)/2);
    var unique = Math.round(Math.random()*1000);
    window.uloopFacebookPopup = window.open(AJAXBaseURL+'/fc.login.php?unique='+unique+AJAXSessionParam+'&callback_func='+callbackFunction,'campusFCPopup','top=305,left='+Width+',width=436,height=367,resizable=no,menubar=no,scrollbars=no,status=no,location=no');
    window.uloopFacebookPopupWatchDogTimer = setInterval(function(){
            if(window.uloopFacebookPopup==null||window.uloopFacebookPopup.closed){ 
                window.clearInterval(window.uloopFacebookPopupWatchDogTimer);
				
                                if(noReload)
					jQ('#sgn_in_posting_popup .popup_close_button').click();
				else{
                                        if(redirect){
                                            if(loginRegisterCallback)
                                                loginRegisterCallback('login', redirect,'','');
                                        }
                                        else{
                                            location.reload();
                                        }
                                }
			
            }
            
            if(window.campusApiHideAllPopups)
                window.campusApiHideAllPopups();
            
           
            
            if( fromPosring ){
                var sessionParams = AJAXSessionParam;
                var unique = Math.round(Math.random()*1000);
                var URL = AJAXBaseURL+'/ajaxapi.php?update_posting_acount_info=1'+sessionParams+'&unique='+unique;
                dhtmlCampusAveLoadScript(URL);
            }
            
        },100);
    
    
}


function ForgotPasswordForm()
{
    var popup_id = "forgot_form";
    popup
    ({
            popup_id:popup_id,
            speed:100
    });
}

function Logout(){
    popup
    ({
            popup_id:"logout_info",
            speed:100,
            close_button:false
    });
    jQ.ajax({
            url: "index.php",
            data: 'action=logout',
            dataType: "html",
            type: "POST",
            success: function(date){
                   setTimeout(function(){
                       location.reload();
                       //window.location = '/';
                   }, 200);
            }
    });return false;
    
}

function submitStZip(){
    jQ('#get_zip').remove();
}

function buildStUnit(id, display){
    if(display){
        jQ('.st_unit_size_table').css('display','none');
        jQ('#desc_reserv_'+id).css('display','block');
        jQ('.hide_block').css('display','block');
    }
    else{
        jQ('.st_unit_size_table').css('display','block');
        jQ('#desc_reserv_'+id).css('display','none');
        jQ('.hide_block').css('display','none');
    }
    return false;
}

/*SUBSCRIBE*/
function Subscribe(this_elem, id_form){  
           this_elem = jQ(this_elem).parents('#follow');
           jQ(this_elem).find('.reply_sended').remove();
          
           var email = jQ(this_elem).find(".email_input").get(0);    
           var emailERROR = jQ(this_elem).find("#ad-info-email-error").get(0);
           
           if(!validationEmail(email, emailERROR))
                return false;
           else {
               if(id_form == 1){
                   jQ('.follow_sm_preload_sidebar').fadeIn(300);
               }
               else{
                   jQ('.follow_sm_preload').fadeIn(300);
               }
               var action = jQ(this_elem).find('#subscribe_submit').attr('onclick');
               jQ(this_elem).find('#subscribe_submit').attr('onclick','');
               jQ(this_elem).attr('disabled',true);
               jQ.ajax({
                            url: "index.php",
                            data: jQ(this_elem).serialize()+'&id_fotm='+id_form,
                            dataType: "html",
                            type: "POST",
                            cache: "false",
                            success: function(date){
                                    jQ(this_elem).find('#ad-info-email-error').empty();
                                    jQ(this_elem).find('#ad-info-email-error').append('<p class="reply_sended" style="color:#388908; font-size:12px; font-weight:bold; clear:left;"></p>').css("visibility","visible");
                                    //jQ(this_elem).find('#ad-info-email-error p').css("margin-top","-31px");
                                    jQ(this_elem).find('.reply_sended').text(date);
                                    jQ(this_elem).attr('disabled',false);
                                    if(id_form == 1){
                                        jQ('.follow_sm_preload_sidebar').fadeOut(300);
                                    }
                                    else{
                                        jQ('.follow_sm_preload').fadeOut(300);
                                    }
                                    jQ(this_elem).find('#subscribe_submit').attr('onclick',action);
                            },
                            error: function(){
                                    jQ(this_elem).find('#subscribe_submit').attr('onclick',action);
                            }
                   });
                                      
                   return false;
           }
           
           return false;
}


/*REPLY BY EMAIL (RESPONSE AND REGISTRATION)*/
    //type = 2 - sublet
function ReplyByEmail(this_elem, type){
          
           var parent_form = jQ(this_elem).parents(".reply");

           var firstName = jQ(parent_form).find("#ad-info-first-name").get(0);
           var firstNameERROR = jQ(parent_form).find("#ad-info-first-name-error").get(0);
           if(!validationInput(firstName, firstNameERROR))
                return false;
           
           var lastName = jQ(parent_form).find("#ad-info-last-name").get(0);
           var lastNameERROR = jQ(parent_form).find("#ad-info-last-name-error").get(0);
           if(!validationInput(lastName, lastNameERROR))
                return false;
           
           if(type==2){
                var phone1 = jQ(parent_form).find("input[name$='phone1']").get(0);
                var phone2 = jQ(parent_form).find("input[name$='phone2']").get(0);
                var phone3 = jQ(parent_form).find("input[name$='phone3']").get(0);
                var phoneERROR = jQ(parent_form).find("#ad-info-phone-error").get(0);
                if(!validationPhone(phone1, phone2, phone3, phoneERROR))
                    return false;
           }
                      
           var email = jQ(parent_form).find("#ad-info-email").get(0);
           var emailERROR = jQ(parent_form).find("#ad-info-email-error").get(0);
           if(!validationEmail(email, emailERROR))
                return false;
            
            
           var message = jQ(parent_form).find("#ad-info-message").get(0);
           var messageERROR = jQ(parent_form).find("#ad-info-message-error").get(0);
           if(!validationInput(message, messageERROR))
                return false;
          
          
           var agree = jQ(parent_form).find("#agree").get(0);
		   
           /*var agreeERROR = jQ(parent_form).find("#ad-info-agree-error").get(0);
           
           
           if(!jQ(agree).is(':checked'))
           {
                    CMPS_SetErrorText_noneScroll(agreeERROR, jQ(agreeERROR).text());
                    agree.focus();
                    agree.onchange = function()
                    {
                        if(agreeERROR)
                        {
                            agreeERROR.style.visibility = 'hidden';
                            this.onchange=null;
                        } 
                    }
                    return false;
           }*/
            jQ(this_elem).attr('disabled',true);
            jQ(".orange_button_h_36").attr('disabled',true);
            jQ(this_elem).parents(".reply").prepend('<div id="modalW" style="width:630px; background: rgba(66, 66, 66, 0.35); margin:-15px 0 -0 -19px; position:absolute;"></div>');
            jQ('#modalW').height(jQ(this_elem).parents(".reply").height()+30);
            jQ(this_elem).parents(".reply").prepend('<div id="preloader" style="position:absolute; margin: 70px 0 0 290px;"><img src="'+imagesDomain+'/img/preloader.gif"/></div>');
            
            jQ.ajax({
                url: "index.php",
                data: jQ(this_elem).parents(".reply").serialize()+"&typeReplyForm="+type+"&id_listing="+jQ("#listing_id").text()+"&agree="+jQ(agree).is(':checked'),
                dataType: "html",
                type: "POST",
                cache: "false",
                success: function(data){ 
                        
                    if(data)
                        var data = JSON.parse(data);
    
                    if(data.response){
                        if(data.content.length)
                            jQ('body').append(data.content);
                       /* 
                        if(data.conversion.length){
                             jQ('body').append('<div id="reply_conversion">'+data.conversion+'</div>');
                        }*/
                        
                        jQ(".campus-posting-input").not(':button, :submit, :reset, :hidden').val('');
                        jQ(this_elem).parents('.reply-form-special, .reply-form').slideUp();
                        jQ(this_elem).parents(".reply").parent().parent().after('<p class="reply_sended" style="color:#388908; padding-top:10px; font-size:19px; font-weight:bold;">Success, your message has been sent!</p>');
                    }
                    else{
                        jQ(this_elem).parents('.reply-form-special, .reply-form').slideUp();
                        
                        if(data.error){
                            jQ(this_elem).parents(".reply").parent().parent().after('<p class="reply_sended" style="color:#FF4026; padding-top:10px; font-size:19px; font-weight:bold;">Form submit error: '+data.error+'</p>');
                        }
                        else
                            jQ(this_elem).parents(".reply").parent().parent().after('<p class="reply_sended" style="color:#FF4026; padding-top:10px; font-size:19px; font-weight:bold;">Error in completing the form, please try again</p>');
                    }
                    jQ("#preloader").remove();
                    jQ("#modalW").remove();
                    jQ(this_elem).attr('disabled',false);
                    jQ(".orange_button_h_36").attr('disabled',false);
                }
            });	
    return false;
} 


function sendReserveStorageUnit(){
    jQ('.st_result').empty();
    jQ('.campus-posting-error-message').css('visibility','hidden');
    
    var firstName = jQ("input[name$='name']").get(0);
    var firstNameERROR = jQ('.hide_block form').find("#ad-info-name-error").get(0);
    if(!validationInput(firstName, firstNameERROR))
        return false;
    
    var lastName = jQ("input[name$='last_name']").get(0);
    var lastNameERROR = jQ('.hide_block form').find("#ad-info-last-name-error").get(0);
    if(!validationInput(lastName, lastNameERROR))
        return false;
    
    var email = jQ("input[name$='email']").get(0);
    var emailERROR = jQ('.hide_block form').find("#ad-info-email-error").get(0);
    if(!validationEmail(email, emailERROR))
        return false;
    
    var phone1 = jQ("input[name$='phone1']").get(0);
    var phone2 = jQ("input[name$='phone2']").get(0);
    var phone3 = jQ("input[name$='phone3']").get(0);
    var phoneERROR = jQ('.hide_block form').find("#ad-info-phone-error").get(0);
    if(!validationPhone(phone1, phone2, phone3, phoneERROR))
        return false;
    
    var startDate = jQ("input[name$='start_date']").get(0);
    var startDateERROR = jQ('.hide_block form').find("#ad-info-date-error").get(0);
    if(!validationInput(startDate, startDateERROR))
        return false;
    
    jQ.ajax({
                type: "POST",
                url: "index.php",
                data:jQ('.hide_block form').serialize(),
                dataType:'json',
                success: function(data){
                    switch(data.error_type){
                        case 0: 
                             jQ('.st_result').append('<b>Success. Storage was reserved. Your confirmation code is '+data.mess+'</b><br/><br/>').css({'color':'green', 'display':'block'});
                            break;
                        case 1: 
                            jQ('.st_result').append('<b>'+data.mess+'!</b><br/><br/>').css({'color':'red', 'display':'block'});
                            break;
                        case 2:
                            jQ('.st_result').append('<b>Error in filling out the fields!</b><br/><br/>').css({'color':'red', 'display':'block'});
                            break;
                    }
                }
            });
    return false;
}


function page_load_storage(location){
    var popup_id = "get_zip";
    jQ("#footer").append("<div class=\"popup_overlay\" id=\"popup_overlay\"></div><div class=\"super_popup\" id=\"get_zip\"><div class=\"sgn_in_popup\"><h2>Please enter your zip code: </h2><form name=\"zip_for_storage\" class=\"popup_form only_num \" id=\"zip_for_storage_form\" action=\""+location+"\" method=\"post\" onsubmit=\"return submitStZip();\"><div><span style=\"color:red; font-size: 14px; display:none;\" id=\"ZipForStorage_error\"> Please enter your zip code!</span><input type=\"text\" name=\"st_zip\" class=\"popup_input\" size=\"35\"/><div class=\"clear\"></div><input type=\"submit\" name=\"Input\" value=\"Submit\" class=\"orange_button_h_36\"/><br/><br/></div></form></div></div>");
    popup
    ({
            popup_id:popup_id,
            speed:100,
            close_button:false,
            hide_on_click: false
    }); 
}


jQ(window).resize(function() {
// top menu responsive tabs	
	var w = jQ(window).width();
	if(w > 969){
		jQ('.main_menu_bottom').hide(); 
	};
	if (w < 500){
		jQ('.main_school_select #state :first').text('State');
		jQ('.main_school_select #school :first').text('University');
	}else{
		jQ('.main_school_select #state :first').text('Select a State');
		jQ('.main_school_select #school :first').text('Select your University');	
	};
  
  });
jQ(document).ready(function(){
  
// if no image
/*
jQ('.detail_main_image img').error(function(){
        jQ(this).attr('src', '/img/no-image-small.jpg');
        var banner = "<div class='banner_336x280_center'>\
                            <script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>\
                                <!-- All other sections - 336x280 -->\
                                <ins class='adsbygoogle'\
                                     style='display:inline-block;width:336px;height:280px'\
                                     data-ad-client='ca-pub-1698388548110737'\
                                     data-ad-slot='2034686390'></ins>\
                                <script>\
                                (adsbygoogle = window.adsbygoogle || []).push({});\
                                </script>\
                        </div>";
        
        jQ('.detail_main_image').html(banner);
});*/

jQ('.post .post_image img, .news_author_blok img.photo, .block_for_image .display_table a img, .detail_small_images img, .detail_main_image img').error(function(){
	jQ(this).attr('src', imagesDomain+'/img/no-image-small.jpg');
});

jQ('.post_rentbooks .block_for_image .display_table a img, .right_links_item .block_for_image img').error(function(){
	jQ(this).attr('src', imagesDomain+'/img/no-image-small2.jpg');
});

	//var state = jQ('.main_school_select #state :first').text();
	var w = jQ(window).width();
	if (w < 500){
		jQ('.main_school_select #state :first').text('State');
		jQ('.main_school_select #school :first').text('University');
	};

// top menu responsive tabs start				
	var k = 0;
	jQ(".left_menu li").click(function(){
	 var w = jQ(window).width();
	 if(w < 969 && jQ.support.opacity){
		jQ(this).find("a").prop("href", "javascript: void(0);");
		jQ('.main_menu_bottom_l').hide();
				
		 if (k == 0){jQ('.main_menu_bottom_r').slideDown(300); k = 1;}
		else if (k == 1) {jQ('.main_menu_bottom_r').slideUp(300); k = 0;}
		else{jQ('.main_menu_bottom_r').slideDown(300); k = 1;};
		} 
		else{
		var lstLink = jQ(".news_menu_bottom li:last").find("a").attr('href');
		jQ('.main_menu_bottom_r').hide();
		//jQ(".left_menu").find("a:first").prop("href", lstLink);
		};
	});
	
	jQ(".right_menu li, #right_short_menu .menu_main_item li").click(function(){
	var w = jQ(window).width();
	if(w < 969 && jQ.support.opacity){
		jQ(this).find("a").prop("href", "javascript: void(0);");
		jQ('.main_menu_bottom_r').hide();
		
		if (k == 0){jQ('.main_menu_bottom_l').slideDown(300); k = 2;}
		else if (k == 2) {jQ('.main_menu_bottom_l').slideUp(300); k = 0;}
		else{jQ('.main_menu_bottom_l').slideDown(300); k = 2;}; 
		}
		else{
		var lstLink = jQ(".class_menu_bottom li:last").find("a").attr('href');
		jQ('.main_menu_bottom_l').hide();
		jQ(".right_menu").find("a:first").prop("href", lstLink);
		};
	});
// top menu responsive tabs end
   
    if(jQ('.NumGroup').length){
        jQ('.NumGroup').groupinputs();
    }
    
    jQ(".log_in").click(function(event){
		var popup_id = jQ(this).attr("rel");
		
		popup({
			popup_id:popup_id
        });
		return false;
    });
    
    
	//OTHER COLLEGES
	(function(){
		jQ('.show_schools_list').each(function(){
			var i = 0;
			jQ(this).click(function(){
				if(jQ(this).next('.schools_list').is(":visible")){
                                    jQ(this).css("background","url("+imagesDomain+"/img/arrow_list_sp.png) no-repeat 0px 5px");
                                    jQ(this).next('.schools_list').slideUp(500);
                                }
                                else{
                                    jQ('.right_content_list li .show_schools_list').css("background","url("+imagesDomain+"/img/arrow_list_sp.png) no-repeat 0px 5px");
                                    jQ(this).css("background","url("+imagesDomain+"/img/arrow_list_sp.png) no-repeat 0px -10px"); 
                                    jQ('.schools_list').slideUp(500);
                                    jQ(this).next('.schools_list').slideDown(500);
                                }
				return false;
			});
		});

	})();
	
	jQ('.school-state-link').click(function(){
		
		var container = jQ(this).next('.school-state-container').get(0);
		var clicked_link = jQ(this).get(0);
		
		if(!jQ(container).children().length)
		{
			
			
			var state = jQ(this).attr('rel');
			jQ(clicked_link).after('<div class="school-state-preloader" align="center"><img src="'+imagesDomain+'/img/small-loading-white.gif"/></div>');

			jQ.ajax({
			  url: "ajax_content.php",
			  type: "POST",
			  data: { action : 'get_schools_by_state_json',"state" : state},
			  dataType: "json"
			}).done(function( json ) {

				container.innerHTML = '';
				for (key in json) {
					var school = json[key];
					var link = document.createElement('a');

					link.setAttribute('href',school.URL);
					link.innerHTML = school.Title;
					container.appendChild(link);	
				}
				
				jQ('.school-state-preloader').remove();
				
				if(jQ(container).is(":visible")){
					jQ(container).slideUp(500);
				}
				else{
					jQ('.school-state-container').slideUp(300);
					jQ(container).slideDown(500);
				}		
				
			});
		}
		else
		{
			if(jQ(container).is(":visible")){
					jQ(container).slideUp(500);
			}
			else{
				jQ('.school-state-container').slideUp(300);
				jQ(container).slideDown(500);
			}	
		}
		
		
		return false;
	});
	
	//
	jQ('*[options="login-required"]').click(function(){
		showLoginDialog();
		return false;
	});

        //REPLY FORM SPECIAL (SUBLET)
	
	jQ('body').on('click','.top_reply .reply-button-special', function(){
		jQ('.top_reply_form .reply-form-special').slideToggle(300);
		//jQ('.top_reply_form .reply-form-special').slideDown();
                jQ("p.reply_sended").remove();
                
	});
	jQ('body').on('click', '.bottom_reply .reply-button-special', function(){
                jQ('.bottom_reply_form .reply-form-special').slideToggle(300);
		//jQ('.bottom_reply_form .reply-form-special').slideDown();
		//jQ('.top_reply_form .reply-form-special').slideUp();
                jQ("p.reply_sended").remove();
	});
	jQ('body').on('click', '.cancel_reply', function(){
                jQuery("#preloader").remove();
		jQ(this).parents('.reply-form-special').slideUp();
		return false;
	});


	//REPLY FORM 
	
	jQ('body').on('click', '.top_reply .reply-button', function(){
		jQ('.top_reply_form .reply-form').slideToggle(300);
		//jQ('.bottom_reply_form .reply-form').slideUp();
                jQ("p.reply_sended").remove();
                
	});
	jQ('body').on('click', '.bottom_reply .reply-button', function(){
                jQ('.bottom_reply_form .reply-form').slideToggle(300);
		//jQ('.bottom_reply_form .reply-form').slideDown();
		//jQ('.top_reply_form .reply-form').slideUp();
                jQ("p.reply_sended").remove();
	});
	jQ('body').on('click', '.cancel_reply', function(){
                jQuery("#preloader").remove();
		jQ(this).parents('.reply-form').slideUp();
		return false;
	});
	
	//POSTING AD CUSTOM FIELDS
	
	AdPostingCustomFields();
	
	jQ('#ad-global-channel').change(function(){
		AdPostingCustomFields();
	});
	jQ('#ad-main-subcategories-list').change(function(){
		AdPostingCustomFields();
	});
	
	
	//STAR INPUT
	
	jQ('.star_input').each(function(index,elem){
		jQ(elem).wrap('<span class="star_cont" />');
		jQ(elem).parent().append('<div class="star_empty star_button"></div>');
		jQ(elem).hide();
	});
	
	
	jQ('.star_button').click(function(){
		var name=  jQ(this).parent().find('input').attr('name');
		var value=  jQ(this).parent().find('input').attr('value');
		jQ('.star_cont input[name="'+name+'"]').prop('checked', false);
		jQ(this).parent().find('input').prop('checked', true);

		jQ('.star_cont input[name="'+name+'"]').each(function(index,elem){
	
			if(jQ(elem).attr('value') <= value)
			{
				jQ(elem).parent().find('.star_button').removeClass('star_empty').addClass('star_full');
			}
			else
			{
				jQ(elem).parent().find('.star_button').removeClass('star_full').addClass('star_empty');
			}
				
		});
		
	});
	
	//FROM VALIDATION
	
	jQ('.validate_form').submit(function(){
	
		var form = this;
		if( jQ(form).find('input[name="validated"]').val() == 'true' )
		{
			return true;
		}
		
		jQ.ajax({
			type: "POST",
			url: "index.php",
			data:jQ(this).serialize()+'&validate=true',
			dataType: "json",
			success: function(json){
				if(json.success){
					jQ(form).append('<input type="hidden" name="validated" value="true">');
					jQ(form).submit();
				}
				else{
					//campus-posting-error-message
					jQ('.validator_error').remove();
					jQ('.campus-posting-error-message').hide();
					if(jQ('#'+json.field_name+'-error').length)
					{
						jQ('#'+json.field_name+'-error').html(json.message);
						jQ('#'+json.field_name+'-error').show();
						CMPS_blink($(json.field_name+'-error'));
						CMPS_ScrollToElement($(json.field_name+'-error'));
					}
					else
					{
						jQ('.validate_form *[name="'+json.field_name+'"]').after('<span id="'+json.field_name+'-error" class="campus-posting-error-message validator_error" style="visibility: visible;display:block;">'+json.message+'</span>');

						CMPS_blink($(json.field_name+'-error'));
						CMPS_ScrollToElement($(json.field_name+'-error'));
					}
				}
			}
		});

		return false;
	});
	
	
	jQ('#sidebar_department').change(function(){
		if(jQ(this).val() != '0')
			window.location = '/professors/?'+jQ(this).attr('name')+'='+jQ(this).val();
		else
			window.location = '/professors/';
	});
        
        jQ('#sidebarF_state').change(function(){
            jQ("#sidebarF_college").children().remove();
		jQ.ajax({
                url: "index.php",
                data: jQ("#college_filter").serialize(),
                dataType: "html",
                type: "POST",
                cache: "false",
                success: function(date){
                    jQ("#sidebarF_college").append(date);
                }
            });
            return false;
	});
        
        jQ('#sidebarF_college').change(function(){
                if(jQ(this).val() != '0')
			window.location = '/news-team/?state='+jQ("#sidebarF_state option:selected").val()+'&'+jQ(this).attr('name')+'='+jQ(this).val();
		else
			window.location = '/news-team/';
	});
	
	
       jQ('body').on('click','.right_to_edit .delete_ads', function(){
           var URL = AJAXBaseURL+'/ajaxapi.php?delete_from_page=1&ads[]='+jQ('.right_to_edit .delete_ads').attr('id');
           dhtmlCampusAveLoadScript(URL);           
           return false;
       });
       
       jQ('body').on('click','.right_to_edit .delete_news', function(){
           var URL = AJAXBaseURL+'/ajaxapi.php?delete_news=1&news='+jQ('.right_to_edit .delete_news').attr('id');
           dhtmlCampusAveLoadScript(URL);           
           return false;
       });
       
       jQ('body').on('click','.right_to_edit .delete_professor', function(){
           var URL = AJAXBaseURL+'/ajaxapi.php?delete_professor=1&professor='+jQ('.right_to_edit .delete_professor').attr('id');
           dhtmlCampusAveLoadScript(URL);           
           return false;
       });
       
       jQ("#ad-info-phoneP1, #ad-info-phoneP2, #ad-info-phoneP3, input[name$='phone1'], input[name$='phone2'], input[name$='phone3']").keydown(function(event) {
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
                (event.keyCode == 65 && event.ctrlKey === true) || 
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    return false; 
                }
            }
        });
       
       
       jQ('#form-join-news .orange_button_h_44').click(function(){

            var username = jQ('#form-join-news').find("#ad-info-username").get(0);
            var usernameERROR = jQ('#form-join-news').find("#ad-info-username-error").get(0);
            
            var email = jQ('#form-join-news').find("#ad-info-email").get(0);
            var emailERROR = jQ('#form-join-news').find("#ad-info-email-error").get(0);
            
            var firstName = jQ('#form-join-news').find("#ad-info-first-name").get(0);
            var firstNameERROR = jQ('#form-join-news').find("#ad-info-first-name-error").get(0);
            
            var lastName = jQ('#form-join-news').find("#ad-info-last-name").get(0);
            var lastNameERROR = jQ('#form-join-news').find("#ad-info-last-name-error").get(0);
            
            
            var phoneP1 = jQ('#form-join-news').find("#ad-info-phoneP1").get(0);
            var phoneP2 = jQ('#form-join-news').find("#ad-info-phoneP2").get(0);
            var phoneP3 = jQ('#form-join-news').find("#ad-info-phoneP3").get(0);
            var phoneERROR = jQ('#form-join-news').find("#ad-info-phone-error").get(0);
            
            
            var major = jQ('#form-join-news').find("#ad-info-major").get(0);
            var majorERROR = jQ('#form-join-news').find("#ad-info-major-error").get(0);
            
            var previousWriting = jQ('#form-join-news').find("#ad-info-previous-writing").get(0);
            var previousWritingERROR = jQ('#form-join-news').find("#ad-info-previous-writing-error").get(0);
            
            var experience = jQ('#form-join-news').find("#ad-info-experience").get(0);
            var experienceERROR = jQ('#form-join-news').find("#ad-info-experience-error").get(0);
        
            if(username.value.length===0 || CMPS_hasHTMLTags(username.value))
            {
                    jQ('#ad-info-username-error').parent('div').css("display", "block");
                    if($('ad-info-username').value.length===0)
                        CMPS_SetErrorText_noneScroll(usernameERROR, jQ(usernameERROR).text());
                    else if(CMPS_hasHTMLTags(username.value))
                        CMPS_SetErrorText_noneScroll(usernameERROR,' Please enter text only');
                    username.focus();
                    username.onkeydown = function()
                    { 
                        if(usernameERROR) 
                        {
                            jQ('#ad-info-username-error').parent('div').css("display", "none");
                            usernameERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            if(email.value.length===0 || CMPS_hasHTMLTags(email.value) || !isValidEmailAddress(email.value))
            {
                    jQ('#ad-info-email-error').parent('div').css("display", "block");
                    if($('ad-info-email').value.length===0)
                        CMPS_SetErrorText_noneScroll(emailERROR, jQ(emailERROR).text());
                    else if(CMPS_hasHTMLTags(email.value))
                        CMPS_SetErrorText_noneScroll(emailERROR,' Please enter text only');
                    else if(!isValidEmailAddress(email.value))
                        CMPS_SetErrorText_noneScroll(emailERROR,' Please enter correct email');
                    email.focus();
                    email.onkeydown = function()
                    { 
                        if(emailERROR) 
                        {
                            jQ('#ad-info-email-error').parent('div').css("display", "none");
                            emailERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            if(firstName.value.length===0 || CMPS_hasHTMLTags(firstName.value))
            {
                    jQ('#ad-info-first-name-error').parent('div').css("display", "block");
                    if($('ad-info-first-name').value.length===0)
                        CMPS_SetErrorText_noneScroll(firstNameERROR, jQ(firstNameERROR).text());
                    else if(CMPS_hasHTMLTags(firstName.value))
                        CMPS_SetErrorText_noneScroll(firstNameERROR,' Please enter text only');
                    firstName.focus();
                    firstName.onkeydown = function()
                    { 
                        if(firstNameERROR) 
                        {
                            jQ('#ad-info-first-name-error').parent('div').css("display", "none");
                            firstNameERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            if(lastName.value.length===0 || CMPS_hasHTMLTags(lastName.value))
            {
                    jQ('#ad-info-last-name-error').parent('div').css("display", "block");
                    if($('ad-info-last-name').value.length===0)
                        CMPS_SetErrorText_noneScroll(lastNameERROR, jQ(lastNameERROR).text());
                    else if(CMPS_hasHTMLTags(lastName.value))
                        CMPS_SetErrorText_noneScroll(lastNameERROR,' Please enter text only');
                    lastName.focus();
                    lastName.onkeydown = function()
                    { 
                        if(lastNameERROR) 
                        {
                            jQ('#ad-info-last-name-error').parent('div').css("display", "none");
                            lastNameERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            var phoneP1 = jQ('#form-join-news').find("#ad-info-phoneP1").get(0);
            var phoneP2 = jQ('#form-join-news').find("#ad-info-phoneP2").get(0);
            var phoneP3 = jQ('#form-join-news').find("#ad-info-phoneP3").get(0);
            var phoneERROR = jQ('#form-join-news').find("#ad-info-phone-error").get(0);
            
            if(phoneP1.value.length===0 || CMPS_hasHTMLTags(phoneP1.value) || $('ad-info-phoneP1').value.length<3 || 
               phoneP2.value.length===0 || CMPS_hasHTMLTags(phoneP2.value) || $('ad-info-phoneP2').value.length<3 || 
               phoneP3.value.length===0 || CMPS_hasHTMLTags(phoneP3.value) || $('ad-info-phoneP3').value.length<4)
            {
                    jQ('#ad-info-phone-error').parent('div').css("display", "block");
                    if($('ad-info-phoneP1').value.length===0 || $('ad-info-phoneP1').value.length<3){
                        $error_len=1;
                        phoneP1.focus();
                    }
                    else if(CMPS_hasHTMLTags(phoneP1.value)){
                        $error_tag=1;
                        phoneP1.focus();
                    }
                    else if($('ad-info-phoneP2').value.length===0 || $('ad-info-phoneP2').value.length<3){
                        $error_len=1;
                        phoneP2.focus();
                    }
                    else if(CMPS_hasHTMLTags(phoneP2.value)){
                        $error_tag=1;
                        phoneP2.focus();
                    }
                    else if($('ad-info-phoneP3').value.length===0 || $('ad-info-phoneP3').value.length<4){
                        $error_len=1;
                        phoneP3.focus();
                    }
                    else if(CMPS_hasHTMLTags(phoneP3.value)){
                        $error_tag=1;
                        phoneP3.focus();
                    }
                    
                    if($error_len)
                        CMPS_SetErrorText_noneScroll(phoneERROR, jQ(phoneERROR).text());
                    else if($error_tag)
                        CMPS_SetErrorText_noneScroll(phoneERROR,' Please enter text only');
                    
                    phoneP1.onkeydown = function()
                    { 
                        if(phoneERROR) 
                        {
                            jQ('#ad-info-phone-error').parent('div').css("display", "none");
                            phoneERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    phoneP2.onkeydown = function()
                    { 
                        if(phoneERROR) 
                        {
                            jQ('#ad-info-phone-error').parent('div').css("display", "none");
                            phoneERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    phoneP3.onkeydown = function()
                    { 
                        if(phoneERROR) 
                        {
                            jQ('#ad-info-phone-error').parent('div').css("display", "none");
                            phoneERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            if(major.value.length===0 || CMPS_hasHTMLTags(major.value))
            {
                    jQ('#ad-info-major-error').parent('div').css("display", "block");
                    if($('ad-info-major').value.length===0)
                        CMPS_SetErrorText_noneScroll(majorERROR, jQ(majorERROR).text());
                    else if(CMPS_hasHTMLTags(major.value))
                        CMPS_SetErrorText_noneScroll(majorERROR,' Please enter text only');
                    major.focus();
                    major.onkeydown = function()
                    { 
                        if(majorERROR) 
                        {
                            jQ('#ad-info-major-error').parent('div').css("display", "none");
                            majorERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
            
            if(previousWriting.value.length===0 || CMPS_hasHTMLTags(previousWriting.value))
            {
                    jQ('#ad-info-previous-writing-error').parent('div').css("display", "block");
                    if($('ad-info-previous-writing').value.length===0)
                        CMPS_SetErrorText_noneScroll(previousWritingERROR, jQ(previousWritingERROR).text());
                    else if(CMPS_hasHTMLTags(previousWriting.value))
                        CMPS_SetErrorText_noneScroll(previousWritingERROR,' Please enter text only');
                    previousWriting.focus();
                    previousWriting.onkeydown = function()
                    { 
                        if(previousWritingERROR) 
                        {
                            jQ('#ad-info-previous-writing-error').parent('div').css("display", "none");
                            previousWritingERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }
           
            if(experience.value.length===0 || CMPS_hasHTMLTags(experience.value))
            {
                    jQ('#ad-info-experience-error').parent('div').css("display", "block");
                    if($('ad-info-experience').value.length===0)
                        CMPS_SetErrorText_noneScroll(experienceERROR, jQ(experienceERROR).text());
                    else if(CMPS_hasHTMLTags(experience.value))
                        CMPS_SetErrorText_noneScroll(experienceERROR,' Please enter text only');
                    experience.focus();
                    experience.onkeydown = function()
                    { 
                        if(experienceERROR) 
                        {
                            jQ('#ad-info-experience-error').parent('div').css("display", "none");
                            experienceERROR.style.visibility = 'hidden';
                            this.onkeydown=null;
                        } 
                    }
                    return false;
            }

            jQ.ajax({
                url: "index.php",
                data: jQ("#form-join-news").serialize(),
                dataType: "html",
                type: "POST",
                cache: "false",
                success: function(data){
                    
                    data = ~~data;
                    
                    switch(data){
                        case 5:{
                            jQ('#ad-info-username-error').parent('div').css("display", "block");
                                CMPS_SetErrorText_noneScroll(usernameERROR,' This username is already used, please choose another one.');
                                username.focus();
                                username.onkeydown = function()
                                { 
                                    if(usernameERROR) 
                                    {
                                        jQ('#ad-info-username-error').parent('div').css("display", "none");
                                        usernameERROR.style.visibility = 'hidden';
                                        this.onkeydown=null;
                                    } 
                                }
                        };break;
                        
                        case 6:{
                            jQ('#ad-info-email-error').parent('div').css("display", "block");
                                CMPS_SetErrorText_noneScroll(emailERROR,' This email is already used, please choose another one.');
                                username.focus();
                                username.onkeydown = function()
                                { 
                                    if(emailERROR) 
                                    {
                                        jQ('#ad-info-email-error').parent('div').css("display", "none");
                                        emailERROR.style.visibility = 'hidden';
                                        this.onkeydown=null;
                                    } 
                                }
                        };break;
                        case 1:{
                            jQ('#send-join-news').remove();
                            jQ('.wrap-join-news-form').before('<div id="send-join-news" style="color:#388908; font-size:20px; clear:left; padding:10px; border:1px solid #cecece; margin-bottom: 20px; width:609px;">Thank you for submitting your application. An editor will be in touch when your application has been reviewed.</div>');
                            jQ('body,html').animate({scrollTop: 0}, 400); 
							jQ('#form-join-news').get(0).reset();
                        };break;
                        default: return false;
                    }
                }
               
            });return false;
           
            return false;
           /*
           var lastName = jQ(parent_form).find("#ad-info-last-name").get(0);
           var lastNameERROR = jQ(parent_form).find("#ad-info-last-name-error").get(0);
           
           var email = jQ(parent_form).find("#ad-info-email").get(0);
           var emailERROR = jQ(parent_form).find("#ad-info-email-error").get(0);
           
           var massage = jQ(parent_form).find("#ad-info-massage").get(0);
           var massageERROR = jQ(parent_form).find("#ad-info-massage-error").get(0);
           
           var agree = jQ(parent_form).find("#agree").get(0);
           var agreeERROR = jQ(parent_form).find("#ad-info-agree-error").get(0);
           */
       });
	
});

function sendContactUs(form)
{  
    var username = jQ(form).find("input[name$='name']").get(0);
    var usernameERROR = jQ(form).find("#name-error").get(0);
    if(!validationInput(username, usernameERROR))
        return false;

    var email = jQ(form).find("input[name$='email']").get(0)
    var emailERROR = jQ(form).find("#email-error").get(0);
    if(!validationEmail(email, emailERROR))
        return false;

    var message = jQ(form).find("textarea[name$='message']").get(0);
    var messageERROR = jQ(form).find("#message-error").get(0);
    if(!validationInput(message, messageERROR))
        return false;


    jQ.ajax({
            url: "index.php",
            data: jQ(form).serialize(),
            dataType: "html",
            type: "POST",
            cache: "false",
            success: function(data){
                
                data = ~~data;
                
                if(data === 1){
                    
                    jQ('#contact_us_popup').hide();
                    
                    jQ('body').append('<div class="super_popup" id="contact_info">\
                                                        <div>\
                                                                <h2>Success. Your Message is sent!</h2>\
                                                        </div>\
                                        </div>');

                    popup
                    ({
                                    popup_id:"contact_info",
                                    speed:100,
                                    close_button:false
                    });
                    
                    setTimeout(function()
                    {
                       location.reload();
                    }, 2000);
                }
                else if(data === 2){
                        var contactCheck = jQ(form).find("input[name$='contactCheck']").get(0);
                        var contactCheckERROR = jQ(form).find("#validation-error").get(0);
                        if(!validationShowError(contactCheck, contactCheckERROR))
                                return false;
                }
                else{

                }

            }
    });
                        return false;
}


function AdPostingCustomFields()
{
	jQ('.custom_fields_block').hide();
	jQ('.custom_fields_block').find('input, textarea, button, select').attr('disabled','disabled');
	jQ('.custom_fields_block').find('.campus-posting-error-message').attr('disabled','disabled');
	
	var channelID = jQ('#ad-global-channel').val();
	var categoryID = jQ('#ad-main-subcategories-list').val();

	if(jQ('.channel-'+channelID+'-category-'+categoryID).length)
	{
		jQ('.channel-'+channelID+'-category-'+categoryID).show();
		jQ('.channel-'+channelID+'-category-'+categoryID).find('input, textarea, button, select').removeAttr('disabled');
		jQ('.channel-'+channelID+'-category-'+categoryID).find('.campus-posting-error-message').removeAttr('disabled');
	}
        if(jQ('.channel-'+channelID+'-category-all').length)
	{
		jQ('.channel-'+channelID+'-category-all').show();
		jQ('.channel-'+channelID+'-category-all').find('input, textarea, button, select').removeAttr('disabled');
		jQ('.channel-'+channelID+'-category-all').find('.campus-posting-error-message').removeAttr('disabled')
	}
	
}


jQ(document).ready(function(){
	
	var inputs = document.getElementsByTagName('input');

	for (index = 0; index < inputs.length; ++index) {
		// deal with inputs[index] element.

		if(inputs[index].getAttribute('default-value'))
		{

			inputs[index].value = inputs[index].getAttribute('default-value');
			inputs[index].onfocus = function(){
				if(this.value == this.getAttribute('default-value'))
					this.value = '';
			}
			inputs[index].onblur = function(){
				if(this.value == '')
					this.value = this.getAttribute('default-value');
			}
		}
	}
	
	
    
    jQ("body").on("keydown", ".only_num", function(e){
       return  CampusNumberInputOnKeyPress(e,jQ(this).val());
    });

    var path = window.location.pathname.substring(1);
 	
	if(jQ('#states-links').get(0))
	{
	
		var states_links = jQ('#states-links').get(0).getElementsByTagName('a');


		for (var i=0; i<states_links.length; i++){
			var state_link = states_links[i];
			var option = document.createElement('option');
			option.value = state_link.getAttribute('rel');
			option.innerHTML = state_link.innerHTML;
			$('state').appendChild(option);

			state_link.style.display = 'none';
		}

		$('state').onchange = function (){
			var state = this.value;

			$('school').disabled="disabled";
			$('school').options.length = 0;

			var option = document.createElement('option');
			option.value="";
			option.innerHTML="Select your University";
			$('school').appendChild(option);

			if(state)
			{
				jQ.ajax({
				  url: "ajax_content.php",
				  type: "POST",
				  data: { action : 'get_schools_by_state_json',state : state },
				  dataType: "json"
				}).done(function( json ) {

					for (key in json) {
						var school = json[key];
						var option = document.createElement('option');
						option.setAttribute('value',school.URL);
						option.innerHTML = school.Title;
						$('school').appendChild(option);	
					}

					$('school').removeAttribute('disabled');

				});
			}

		}
		$('go').onclick = function(){
			if($('school').value){
				window.location = $('school').value+'/'+window.location.pathname.substring(1);
			}
		}

	}

	
	if(jQ('#states-links-post').get(0) && jQ('#state-post').get(0))
	{
		var states_links = jQ('#states-links-post').get(0).getElementsByTagName('a');

		for (var i=0; i<states_links.length; i++){
			var state_link = states_links[i];
			var option = document.createElement('option');
			option.value = state_link.getAttribute('rel');
			option.innerHTML = state_link.innerHTML;
			$('state-post').appendChild(option);

			state_link.style.display = 'none';
		}

		$('state-post').onchange = function (){
			var state = this.value;

			$('school-post').disabled="disabled";
			$('school-post').options.length = 0;

			var option = document.createElement('option');
			option.value="";
			option.innerHTML="Select your University";
			$('school-post').appendChild(option);

			if(state)
			{
				jQ.ajax({
				  url: "ajax_content.php",
				  type: "POST",
				  data: { action : 'get_schools_by_state_json',state : state },
				  dataType: "json"
				}).done(function( json ) {

					for (key in json) {
						var school = json[key];
						var option = document.createElement('option');
						option.setAttribute('value',school.URL);
						option.innerHTML = school.Title;
						$('school-post').appendChild(option);	
					}

					$('school-post').removeAttribute('disabled');

				});
			}

		}
		/*var states_links = $('states-links-post').getElementsByTagName('a');
		var schools_links = $('schools-links-post').getElementsByTagName('a');

		for (var i=0; i<states_links.length; i++) {
				var state_link = states_links[i];
				state_link.style.display = 'none';
		}

		for (var i=0; i<schools_links.length; i++) {	
				var school_link = schools_links[i];
				school_link.style.display = 'none';
		}

		for (var i=0; i<states_links.length; i++){
				var state_link = states_links[i];
				var option = document.createElement('option');
				option.value = state_link.getAttribute('rel');
				option.innerHTML = state_link.innerHTML;
				$('state-post').appendChild(option);

				state_link.style.display = 'none';
		}

		$('state-post').onchange = function (){
				var state = this.value;

				var schools = $('school-post').getElementsByTagName('option');
				for(var i=0; i<schools.length; i++){
						var school = schools[i];
						if(i>0){
								school.parentNode.removeChild(school);
						}
						i++;
				}
				$('school-post').options.length = 0;
				var option = document.createElement('option');
				option.value="";
				option.innerHTML="Select your University";
				$('school-post').appendChild(option);
				if(state){
						$('school-post').removeAttribute('disabled');
						for(var i=0; i<schools_links.length; i++){
								var school_link = schools_links[i];
								if(school_link.getAttribute('rel') == state){
										var option = document.createElement('option');
										option.setAttribute('value',school_link.getAttribute('href'));
										option.setAttribute('id',school_link.getAttribute('id'));
										option.innerHTML = school_link.innerHTML;
										$('school-post').appendChild(option);	
								}
						}
				}
				else{
					$('school-post').disabled="disabled";
				}
		}*/
	}
       
  if(jQ('#post_ads').get(0)){ 
    $('post_ads').onclick = function(){ 
            var schoolId = jQ('#register_user_and_choise_school input[name$="school_i"]').val();

            if(!jQ('#school-post option:selected').val().length){
                var state = $("state-post");
                var school = $("school-post");
                var schoolErorr = $("select-school-error");
                CMPS_SetErrorText_noneScroll(schoolErorr, jQ(schoolErorr).text());
                school.focus();
                school.change = function()
                { 
                    if(schoolErorr) 
                    {
                        schoolErorr.style.visibility = 'hidden';
                        this.onkeydown=null;
                    } 
                }
                return false;
            }
            else {
               if(schoolId>0){
                    var selectedSchool = jQ('#school-post option:selected').attr('id').replace('select_','');
                     jQ.ajax({
                             url: "profile.htm",
                             data: "action="+jQ('#register_user_and_choise_school input[name$="action"]').val()+"&newVal="+selectedSchool,
                             type: "POST",
                             dataType: "html",
                             success:function(data){
                                window.location = $('school-post').value+'/post_ads.htm';
                             }
                     }); 
                }
                else{
                    window.location = $('school-post').value+'/post_ads.htm';
                }
            }
            return false;
    }
  }
  
 
  jQ("a[view_external_link]").each(function(){
	  jQ(this).attr('href','/view_external_link.php?'+jQ(this).attr('view_external_link') );
	  if(jQ(this).attr('target') !== '_self'){
                jQ(this).attr('rel','nofollow');
                jQ(this).attr('target','_blank');
            }
  });
  
  
	jQ('html').click(function() {

		if(results_container)
			  jQ(results_container).css("display","none");

	});


jQ('#search_input').keydown(function(event) {
        
        if( event.keyCode === 13 || event.keyCode === 40 || event.keyCode === 38)
        {
            var total_idx = jQ('body').find('#fast_search_results .fast_search_item').length;          

            // enter
            if(event.keyCode === 13)
            {
                jQ('#fast_search_results .fast_search_focus_item .fast_search_image img').click();

            }
            // down
            else if (event.keyCode === 40) 
            {
                jQ('#fast_search_results .fast_search_item').removeClass('fast_search_focus_item');

                search_row_index++;

                if(search_row_index === total_idx){
                    search_row_index = 0;
                }

                var FocusRow = jQ('.fast_search_item:eq(' + search_row_index + ')');
                jQ(FocusRow).addClass('fast_search_focus_item');

            }
            // up
            else if (event.keyCode === 38) 
            {
                jQ('#fast_search_results .fast_search_item').removeClass('fast_search_focus_item');

                search_row_index--;

                if(search_row_index === -1){
                    search_row_index = total_idx-1;
                }

                var FocusRow = jQ('.fast_search_item:eq(' + search_row_index + ')');
                jQ(FocusRow).addClass('fast_search_focus_item');
                return false;
            }
            
            return false;
        }
        // esc if (event.keyCode === 27)
        else 
        {
            jQ('#fast_search_results .fast_search_item').removeClass('fast_search_focus_item');
            search_row_index = -1;
        }
 });

  
jQ(document).ready(function(){
    var left_menu_len = jQ(".left_menu li").length;

    for(i=0; i < left_menu_len; i++){
     var main_menu_width = jQ("#main_menu").width();
     var rl_menu_width = jQ(".left_menu").width() + jQ(".right_menu").width();
     if(main_menu_width < rl_menu_width){
      var left_menu_item = jQ(".left_menu li").eq(left_menu_len-i).prev();

      left_menu_item.hide();
      jQ(".left_menu .more_link .drop_block").prepend(left_menu_item.html());
     }
    }
});
  
  
  jQ("#search_input").keyup(function(){
                
		if(search_value!=jQ(this).val())
		{                    
                    if(!jQ("#fast_search_results").find(".fast_search_overflow").get(0))
                    jQ("#fast_search_results").html("<div class='fast_search_overflow'>"+jQ("#fast_search_results").html()+"</div>"); 

                    clearTimeout(search_timer);

                    search_timer = setTimeout("ProcessFastSearch('"+jQ(this).val()+"')",400);
                    search_value = jQ(this).val();
                        
		}
		
		if(!jQ(this).val().length)
		{
			jQ("#fast_search_results").css('display','none');
		}
			  
  });
  jQ("#search_input").focus(function(){

		if(search_value!=jQ("#search_input").val())
		{
			if(!jQ("#fast_search_results").find(".fast_search_overflow").get(0))
			jQ("#fast_search_results").html("<div class='fast_search_overflow'>"+jQ("#fast_search_results").html()+"</div>"); 

			clearTimeout(search_timer);

			search_timer = setTimeout("ProcessFastSearch('"+jQ(this).val()+"')",400);
			search_value = jQ(this).val();
		}
		else if(search_value==jQ("#search_input").val() && jQ("#fast_search_results").css('display') == 'none' )
		{
			jQ("#fast_search_results").css('display','');
		}
		
		if(!jQ(this).val().length)
		{
			jQ("#fast_search_results").css('display','none');
		}
			  
  });
  
      
  
  jQ("#search_form").submit(function(){
	  
		if(search_value!=jQ("#search_input").val())
		{
			if(!jQ("#fast_search_results").find(".fast_search_overflow").get(0))
			jQ("#fast_search_results").html("<div class='fast_search_overflow'>"+jQ("#fast_search_results").html()+"</div>"); 

			clearTimeout(search_timer);

			ProcessFastSearch(jQ("#search_input").val());
			search_value = jQ("#search_input").val();
		}
		else if(search_value==jQ("#search_input").val() && jQ("#fast_search_results").css('display') == 'none' )
		{
			jQ("#fast_search_results").css('display','');
		}
		
		if(!jQ("#search_input").val().length)
		{
			jQ("#fast_search_results").css('display','none');
		}
		
		return false;
  });

   
});


 function ShowPopup(id_block){
    popup
    ({
            popup_id: id_block,
            speed:100
    });
    return false;
}
 function ShowCustomPopup(id_block,message){
	 
	jQ("#custom_message_container").html(message)
			
    popup
    ({
            popup_id: id_block,
            speed:100
    });
    return false;
}

function confirm_email(login){
    jQ('#send_confirm_msg_info div').html('<h2>Confirmation email has been sent to your email!</h2>');
    jQ.ajax({
            url: "index.php",
            data: 'action=send_confirm_email&login='+login,
            dataType: "html",
            type: "POST",
            success: function(data){
                    
                  data = ~~data;
                
                  if(data==1){
                      ShowPopup('send_confirm_msg_info');
                  }
            }
    });return false;
}

function confirm_pass(login){
    jQ('#send_confirm_msg_info div').html('<h2>Confirmation password has been sent to your email!</h2>');
    jQ.ajax({
            url: "index.php",
            data: 'action=send_confirm_password&login='+login,
            dataType: "html",
            type: "POST",
            success: function(data){
                
                  data = ~~data;
                  
                  if(data==1){
                      ShowPopup('send_confirm_msg_info');
                  }
            }
    });return false;
}
function PrintReceipt(text){
    var w=window.open("","_rcpt", "width=870,height=865");
    w.document.write('<html><head><link href="" rel="stylesheet" type="text\css"></head><body>'+text+'</body></html>');
    w.print();
    return false;
    
}


function ProcessFastSearch(search_str)
{
	
	if(search_str == 'Search') return;
	
	  jQ.ajax({
				url: "ajax_content.php",
				data: "action=get_search_json&s="+search_str,
				type: "POST",
				dataType: "json",
				success:function(json){
				   
				   if((json.classifieds.items.length || json.news.items.length) && search_str.length)
				   {
				   
				   
						if(!results_container)
						{
							results_container = document.createElement('div');
							results_container.className = "fast_search_results";
							results_container.id = "fast_search_results";


							$("search_form").appendChild(results_container);
						}


						jQ(results_container).css("display","");

						jQ(results_container).click(function(event){
							 event.stopPropagation();
						 });

						jQ("#search_form .overflow").click(function(event){
							 event.stopPropagation();
						 });

						while( results_container.hasChildNodes() ){
							 results_container.removeChild(results_container.lastChild);
						 }
						 
						 //NEWS
						
						if(json.news.items.length)
						{
							var news_title = document.createElement('div');
							news_title.className = "fast_search_title";
							news_title.innerHTML = "News ("+json.news.total_found+")";

							results_container.appendChild(news_title);

							for (key in json.news.items) 
							{

								 var item_container = document.createElement('div');
								 item_container.className = "fast_search_item";
                                                                 
                                                                 var item_container_text = document.createElement('div');
								 item_container_text.className = "fast_search_item_text";

								 var item = json.news.items[key];

								 var linkEl = document.createElement('a');
								 linkEl.setAttribute("href",item.link);
								 linkEl.innerHTML = item.title;
								 linkEl.className="fast_search_link";

								 var imgLinkEl = document.createElement('a');
								 imgLinkEl.setAttribute("href",item.link);
								 imgLinkEl.className="fast_search_image";

								 var imgEl = document.createElement('img');
								 if(item.image)
								 {
									 imgEl.setAttribute("src",item.image);
								 }
								 else
								 {
									 imgEl.setAttribute("src",imagesDomain+'/img/no-image-small.jpg');
									 imgEl.className="no-photo";
								 }

								 imgLinkEl.appendChild(imgEl);

								 var additional_html = document.createElement('div');
								 additional_html.innerHTML = item.additional_html;
								 additional_html.className = "fast_search_additional";

								 item_container.appendChild(imgLinkEl);

                                                                 item_container_text.appendChild(linkEl);
                                                                 
                                                                 item_container_text.appendChild(additional_html);

								 item_container.appendChild(item_container_text);

								 var clear = document.createElement('div');
								 clear.className = "clear";
								 item_container.appendChild(clear);

								 results_container.appendChild(item_container);
							}

							var see_all_news_container = document.createElement('div');
							see_all_news_container.className = 'fast_search_see_all';
							var see_all_link = document.createElement('a'); 
							see_all_link.innerHTML = 'See all News results';
							see_all_link.setAttribute("href","/news/?s="+search_str);
							see_all_news_container.appendChild(see_all_link);
							results_container.appendChild(see_all_news_container);
						}

						//CLASSIFIEDS

						if(json.classifieds.items.length)
						{
							
							var classifieds_title = document.createElement('div');
							classifieds_title.className = "fast_search_title";
							classifieds_title.innerHTML = "Classifieds ("+json.classifieds.total_found+")";

							results_container.appendChild(classifieds_title);

							for (key in json.classifieds.items) {

								 var item_container = document.createElement('div');
								 item_container.className = "fast_search_item";
                                                                 
                                                                 var item_container_text = document.createElement('div');
								 item_container_text.className = "fast_search_item_text";

								 var item = json.classifieds.items[key];

								 var linkEl = document.createElement('a');
								 linkEl.setAttribute("href",item.link);
								 linkEl.innerHTML = item.title;
								  if(item.link.indexOf('view_external_link') != -1)
									linkEl.setAttribute("target","_blank");
								 linkEl.className="fast_search_link";

								 var imgLinkEl = document.createElement('a');
								 imgLinkEl.setAttribute("href",item.link);
								 if(item.link.indexOf('view_external_link') != -1)
									imgLinkEl.setAttribute("target","_blank");
								
								 imgLinkEl.className="fast_search_image";

								 var imgEl = document.createElement('img');
								 if(item.image)
								 {
									 imgEl.setAttribute("src",item.image);
                                                                         imgEl.onerror = function(){
                                                                                this.setAttribute("src",imagesDomain+'/img/no-image-small.jpg');
                                                                                this.className="no-photo";
                                                                         }
								 }
								 else
								 {
									 imgEl.setAttribute("src",imagesDomain+'/img/no-image-small.jpg');
									 imgEl.className="no-photo";
								 }

								 imgLinkEl.appendChild(imgEl);


								 var additional_html = document.createElement('div');
								 additional_html.innerHTML = item.additional_html;
								 additional_html.className = "fast_search_additional";

								 item_container.appendChild(imgLinkEl);

                                                                 item_container_text.appendChild(linkEl);
                                                                 
                                                                 item_container_text.appendChild(additional_html);

								 item_container.appendChild(item_container_text);

								 var clear = document.createElement('div');
								 clear.className = "clear";
								 item_container.appendChild(clear);

								 results_container.appendChild(item_container);
							}

							var see_all_classifieds_container = document.createElement('div');
							see_all_classifieds_container.className = 'fast_search_see_all';
							var see_all_link = document.createElement('a'); 
							see_all_link.innerHTML = 'See all Classifieds results';
							see_all_link.setAttribute("href","/classifieds/?s="+search_str);
							see_all_classifieds_container.appendChild(see_all_link);
							results_container.appendChild(see_all_classifieds_container);

						}
						
				   }
				   else
				   {
					   jQ("#fast_search_results").css('display','none');
					   jQ("#fast_search_results").html('');
					   
				   }
				}
		}); 
}



//////////////////////////////////////

var parentElem = '';

function showUploadDialog_HTML5(elem, upload_id, reset, errorFieldId, parrentClass)
{

    var ImputUpload = '';
    parrentClass = jQ(elem).closest(parrentClass).attr('class');
    parentElem = parrentClass.length ? '.'+parrentClass+' ' : '';

    if(upload_id && upload_id.length){
            ImputUpload = jQ(parentElem+'#'+upload_id);
			console.log(ImputUpload);
            if( !ImputUpload )
                return;
    }
    else
       return;

    if( reset ){
            jQ(parentElem+'#'+upload_id).val('');
    }
    
    if( errorFieldId.length ){
            jQ(parentElem+'#'+errorFieldId).html('');
    }
    
    ImputUpload.click();
}

 
function uploadFile_HTML5(fileInput, onSuccess_HTML5, onError_HTML5, onProgress_HTML5, clearFunction)
{
    var file = fileInput.files[0];
    if ( !file ) 
        return;
    
    var xhr = new XMLHttpRequest();
    
    
    xhr.onload = xhr.onerror = function(param) 
    {
        if( this.status != 200 && xhr.responseText.length==0 ) {
              if( onError_HTML5.length ){
                  window[onError_HTML5](this);
              }
              return;
        }

        if( onSuccess_HTML5.length ){
              window[onSuccess_HTML5](xhr.responseText);
        }
    };
    

    if( onProgress_HTML5.length ){
        xhr.upload.onprogress = function(event) {
            window[onProgress_HTML5](clearFunction);
        }
    };
    
    var oMyForm = new FormData();

    oMyForm.append("Filedata", file);

    xhr.open("POST", "upload-files.php");
    xhr.send(oMyForm);
}


function uploadLogoFile_HTML5(fileInput, onSuccess_HTML5, onError_HTML5, onProgress_HTML5, clearFunction)
{
    var file = fileInput.files[0];
    if ( !file ) 
        return;
    
    var xhr = new XMLHttpRequest();
    
    
    xhr.onload = xhr.onerror = function(param) 
    {
        if( this.status != 200 && xhr.responseText.length==0 ) {
              if( onError_HTML5.length ){
                  window[onError_HTML5](this);
              }
              return;
        }

        if( onSuccess_HTML5.length ){
              window[onSuccess_HTML5](xhr.responseText);
        }
    };
    

    if( onProgress_HTML5.length ){
        xhr.upload.onprogress = function(event) {
            window[onProgress_HTML5](clearFunction);
        }
    };
    
    var oMyForm = new FormData();

    oMyForm.append("Filedata", file);

    xhr.open("POST", "upload-logos.php");
    xhr.send(oMyForm);
}

function uploadResumeProgress_HTML5(clearFunction)
{
    if( clearFunction.length )
        window[clearFunction](jQ(parentElem+'#resume_remove'));
    jQ(parentElem+'#resume_preloader').css('display', '');
}

function uploadResumeSuccess_HTML5(response)
{
    jQ(parentElem+'#resume_preloader').css('display', 'none');
     jQ(parentElem+'#error_upload').html('');
     
    if( !response )
        return;
    
    if( response.indexOf('Upload ') !== -1 || response.indexOf(' only') !== -1 ){
        uploadRemoveResume(jQ(parentElem+'#resume_remove'));
        jQ(parentElem+'#error_upload').html(response);
        CMPS_blink(jQ(parentElem+'#error_upload')[0]);
        
    }
    else if( response.indexOf('.pdf') !== -1 )
        jQ(parentElem+'#resume_icon').attr('src', imagesDomain+'/img/icon/pdf_icon.png').css('display', '');
    else if( response.indexOf('.doc') !== -1 || response.indexOf('.docx') !== -1 )
        jQ(parentElem+'#resume_icon').attr('src', imagesDomain+'/img/icon/word_icon.png').css('display', '');
    
    if( response.indexOf('.pdf') !== -1 || response.indexOf('.doc') !== -1 || response.indexOf('.docx') !== -1 ){
        jQ(parentElem+'#resume_view').attr('href', response);
        jQ(parentElem+'#link_resume').attr('value', response);
    }
    
    addResumeIconEvent();
    
}

function uploadError_HTML5()
{
    alert('Error upload file');	
}

function addResumeIconEvent()
{ 
    jQ('body').on('click', parentElem+'#resume_icon', function(elem){
        var currenParentActionElem = jQ(this).closest('.top_reply_form, .bottom_reply_form').attr('class');
        currenParentActionElem = currenParentActionElem.length ? '.'+currenParentActionElem+' ' : '';
        
        if( jQ(currenParentActionElem+'.action_resume').css('display') != 'none' )
            jQ(currenParentActionElem+'.action_resume').fadeOut();
        else  
            jQ(currenParentActionElem+'.action_resume').fadeIn();  
    });
}

function RemoveResumeIconEvent(elem)
{
    var currenParentRemoveElem = '';
    if(elem && elem.length)
        currenParentRemoveElem = jQ(elem).closest('.top_reply_form, .bottom_reply_form').attr('class');
    
    currenParentRemoveElem = currenParentRemoveElem.length ? '.'+currenParentRemoveElem+' ' : '';
    
    jQ('body').off('click', currenParentRemoveElem+'#resume_icon');
    jQ(currenParentRemoveElem+'.action_resume').hide();
}

function uploadRemoveResume(elem)
{
    var currentParentElem = '';
    if(elem && elem.length)
        currentParentElem = elem.closest('.top_reply_form, .bottom_reply_form').attr('class');
    
    currentParentElem = currentParentElem.length ? '.'+currentParentElem+' ' : '';
       
    jQ('body').off('click', currentParentElem+'#resume_icon');
    
    jQ(currentParentElem+'.action_resume').hide();
    jQ(currentParentElem+'#resume_icon').attr('src', imagesDomain+'/img/letters/spacer.png');
    jQ(currentParentElem+'#resume_view').attr('href', '');
    jQ(currentParentElem+'#link_resume').attr('value', '');
}





