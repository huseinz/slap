SLAP STRATEGY SESSION 
Thursday, Feb 4 2016

ARAMARK CAMPAIGN 2016

This is the draft created during the first portion of this strategy
session. During this meeting we primarily fleshed out the composition
of the issues we are going to be focusing on. We also discussed how 
to frame the issues on the petition, which we plan to have completed
by Tuesday. 

We are still in the process of submitting the FOIA request to the 
proper individuals. 

The FOIA request that is going to the communications
director will ask for:

	- The original contract with Aramark that was discussed at the 
	November 21st-ish Board of Trustees meeting. 
	- The current status of the proposed amendment to that contract
	- Bill Merck's communications with Aramark 

The request going to the OSI coordinator will ask for:

	- Cait Zona's emails regarding Aramark

The petition is being written by Derek and Emily. 

We are taking the angle of stating that these issues are things that 
*should* be in the contract rather than asking that UCF & Aramark put 
these in the contract, if that makes sense. We don't want to look like
we're asking them to do something they've already done. 

ISSUES:

	1. Accountability to Students 

		The communications and decisions between UCF and Aramark
		should be publically accessible and students should be 
		actively involved in it. 
		
		A student committee (or some body) that has oversight
		over employment practices, food safety, price changes,
		and venue changes. This body would probably have no 
		actual power over Aramark but is able to report on all
		decisions from Aramark.

	2. Safety Regulations

		Health code regulations regarding food and worker safety
		should be enforced and Aramark should be held accountable 
		and face consequences for violations.


	3. Terms of Employment 

		There should be specific provisions within the contract 
		to ensure the following for all student and non-student
		workers:

			- A negotiated minimum wage 
			- A notice at least two weeks prior to termination
			- A guaranteed nonzero amount of shifts/hours 
			per week 

RESEARCH GAPS:

	- Things requested in the FOIA (Aramark contract and communications 
	between Merck, Zona, and Aramark	
	
	- Central Florida Aramark representatives and management
	
	- Worker needs and wants

	- Health code regulations and violations 

	- Specific Aramark health inspection reports and violations

	- How Aramark has operated on other campuses

	- Have there been similiar deals at other campuses (such as UF)

GOALS:

	Long Term:
		- Aramark Banished From UCF
		- Locally run dining

	Intermediate:
		- The three campaign issues
		- Amendments to the Aramark contract 

	Short Term:
		- Extending the Aramark contract's approval date
		- 2000 petition signatures

ORGANIZATIONAL CONSIDERATIONS:

	Resources:
		- $350 			- Cris' KnightNews connection
		- Free color printing	- Megaphone
		- ~10 core members	- Artistry
		- Tabling space
		- Room reservations

	What we want out of the campaign:
		- 20 more core members
		- 50 auxiliary members
		- $1000
		- 5 new leaders

	Internal Problems:
		- Lack of ownership	- Fear of taking risks
		- Lack of motivation	- Communication issues
		- Lack of membership
		- Lack of initiative
		- Lack of responsibility

CONSTITUENTS, ALLIES, AND OPPONENTS:

	Constituents:
		- SU student workers
		- Dining plan holders
		- Parents of dining plan holders
		- UNITE-HERE
		- Student athletes

	Allies:
		- VOX, JWJ, DDoD, BARC, UFF, SJP
		- SU business owners
		- Students that eat on campus
		- OSI Director Shane Juntunen
		- OSI Asst. Director Matt Betz
		- KoRT Coordinator Katie Marshall
		- SJ&A Director Edwanna Andrews

	Opponents:
		- SGA President Cait Zona
		- Aramark Representatives

TARGETS:

	Primary Target:
	
		- CFO William 'Bill' 'Willy' Merck

	Secondary Target:

		- VP Rick Schell

TACTICS:
	
	- Petitioning
	- Meeting with Bill Merck
	- Picketing/Action at Board of Trustees meetings
	- Open Forum questions
	- Effective boycotts
	- Teach-in on Aramark deal
	- Letter/Pledge drops to Merck's office	
	- Mic checks
	- Action at SGA meetings directed at Zona
	- Have a day mourning the loss of the SU/vendors who will be replaced
	- Informing people of non-Aramark options
	- Writing op-eds and articles on Aramark deal
	- Flyering to new students/parents during orientation and tours
	- Have SU vendors pass out literature
	- Strikes by vendor owners and workers in the SU and Aramark employees
	- Banner drop in SU
	- SGA resolution
	- Marches to BoT
	- Valentines day stuff
	
TIMELINE:

	TBD
