SLAP STRATEGY SESSION 
Thursday, Feb 4 2016

ARAMARK CAMPAIGN 2016

This is the draft created during the first portion of this strategy
session. During this meeting we primarily fleshed out the composition
of the issues we are going to be focusing on. We also discussed how 
to frame the issues on the petition, which we plan to have completed
by Tuesday. 

We are still in the process of submitting the FOIA request to the 
proper individuals. 

The FOIA request that is going to the communications
director will ask for:

	- The original contract with Aramark that was discussed at the 
	November 21st-ish Board of Trustees meeting. 
	- The current status of the proposed amendment to that contract
	- *possibly maybe* Bill Merck's communications with Aramark 

The request going to the OSI coordinator will ask for:

	- Cait Zona's emails regarding Aramark

The petition is being written by Derek and Emily. 

We are taking the angle of stating that these issues are things that 
*should* be in the contract rather than asking that UCF & Aramark put 
these in the contract, if that makes sense. We don't want to look like
we're asking them to do something they've already done. 


The following issues are written in the context 

Issues:

	1. Accountability to Students 

		The communications and decisions between UCF and Aramark
		should be publically accessible and students should be 
		actively involved in it. 
		
		A student committee (or some body) that has oversight
		over employment practices, food safety, price changes,
		and venue changes. This body would probably have no 
		actual power over Aramark but is able to report on all
		decisions from Aramark.

	2. Safety Regulations

		Health code regulations regarding food and worker safety
		should be enforced and Aramark should be held accountable 
		and face consequences for violations.


	3. Terms of Employment 

		There should be specific provisions within the contract 
		to ensure the following for all student and non-student
		workers:

			- A negotiated minimum wage 
			- A notice at least two weeks prior to termination
			- A guaranteed nonzero amount of shifts/hours 
			per week 


This is still a draft that will be completed this Saturday. 
