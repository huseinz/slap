SLAP Meeting Nov 3 2015

Agenda

	Million Student March Details 
	VP Meeting 
	SLAPsgiving 
	JWJ 

		Note: 
		CFPB Pledge (direct) 

			My organization pledges to:

			* Talk to our employees about their options for student loan forgiveness.
			* Help them prove that they work for a public service organization.
			* Check-in with employees annually to make sure they stay on track.

Million Student March 

	- Delivering a large check to Pres. Hitt/Millican Hall 

	- Happening Nov 12 @ ~ 11 AM

	- Make the Check / Finish the Banner Day 
		This Friday @ 3PM, Nick's place 

	- Roles 
		Ofelia and Nicole M will present check.
		Nicole H and Nick will march with check. 

	- Outreach 
		
		Dems -> Cris 
		KFB  -> Ofelia 
		VOX  -> Nicole H

Meeting w/ Rick Schell

	There will be a strategy session for this next Tuesday at 7PM in the library. 

	Endorsements
		~5 professors 
			Scott
			Chelsea x1
			Nicole M x1 
			Ofelia x2
		RSOs
			Ofelia -> KFB, Dems, Rep. 
			SJP, DD, SSA, NORML, NOW, MSA, Screenwriters, anyone really 
			SGA - Nicole H, email 
	
	Note: create agenda 

	Roles 

		Ofelia		Facilitator
		Nicole H	Moderator	 
		Nicole M	Co-facilitator
		Zubir		Notetaker
	
	Demands Draft

		Minimum
			Make the current agreements HR policy so they can be held 
			accountable for maintaining it. (Note: put in clause that states if PSLF
			is no longer existing, then they don't have to do so)

		Middle 
			Negotiating the pledge together, make it HR policy

		Perfect
			Agreeing to what we have now


	Action Plan Draft for supporters
		
		We're going to have a group of supporters during our meeting. 

		- Meet up in front of the library 
		- We then march to Millican Hall, doing chants and holding signs and such,
		and those who are able to will take the stairs up to the third floor. 
		- Supporters will line the walls outside of the lobby.
		- Those who are not attending the meeting itself will stay with the supporters
		and keep them hyped/occupied during the meeting. 
		- Afterwards, after the debrief we do some flyering outside. 

SLAPsgiving
	
	Saturday, Nov 14th @ ~9PM at Nick's place
	$3 or BYOB 

	Jay will make flyers
	Nicole M will bring chips and ice
	Ofelia will fill keg and buy tap
	Nicole H will make event page


