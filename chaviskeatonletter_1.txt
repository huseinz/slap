Dear Dr. Chavis and Ms. Keaton: 

As agreed, we are sending this email to confirm what was decided at the meeting last Friday. 

First and foremost, Human Resources had decided before the meeting that they would add information
regarding Public Service Loan Forgiveness (PSLF) to their employee orientation and add a link
to the program's homepage to their website. We were extremely happy to hear this and agree that
both would be useful to promoting awareness of the program among UCF faculty and staff. 

We are still asking that UCF take the Consumer Financial Protection Bureau pledge to, as outlined
in our letter, "notify all students and employees of loan repayment options, offer two days,
one after each semester, to help students and employees enroll in programs, and keep data on
how many employees are enrolled in loan repayment programs." Both Dr. Chavis and Ms. Keaton
expressed at the meeting that the university is unsure of its stance on the pledge itself and
that neither is in a position to make that ruling themselves. 

At this point, however, Dr. Chavis did suggest sending information concerning PSLF to UCF
employees. Ms. Keaton also suggested perhaps including PSLF information in the exit packet for
graduating UCF students. Additionally, with regards to the component of our letter concerning
information sessions hosted for UCF employees and students, Ms. Keaton offered to talk to her
assistant advisor about incorporating PSLF into UCF's financial literacy modules and the exit
packet given to UCF graduates. To reiterate, though, it is our understanding that HR has already
agreed to include PSLF as a component of their new employee orientation and to reference this
program on their website. As far as we understand, these were the concrete takeaways from the
meeting. 

Lastly, given that much of this topic surrounds Human Resources, Dr. Chavis and Ms. Keaton
offered to talk to that department,  also stating that they would facilitate the discussions
with Human Resources at this point. Specifically, we are interested in hearing HR's stance on
hosting PSLF enrollment days for employees and if they would also be willing to meet with us. As
a last request on this point, we would like to know when Human Resources plans to implement
what they have so far offered and if we could be notified once they have done so. 

Apart from what was decided at the meeting, we wanted to provide you with some examples of other
institutions that have taken this pledge. As noted in the original letter, Palomar College took
this pledge late last year. This college did so not out of high debt or default rates among
their students and alumni, but rather concern for student debt among their employees. Part-time
faculty at the community college had requested that the university take this pledge in order
to increase the number of eligible enrollments among part-time faculty. The City of Cincinnati,
Ohio and the Federal Department of Education have also taken this pledge. It is our thought that
public sector employers ought to align themselves with this initiative and as UCF students we
would like to see our university among the first to do so. 

We would like to thank you both for meeting and discussing this topic with us last Friday. We
are certainly interested in continuing conversations about this program and its role at our
university. Please let us know when you receive word back from the folks you intended to reach
after the meeting. We will follow up with you Monday of next week if we have not already heard
from you by then. 

Thank you, 
Student Labor Action Project at UCF
