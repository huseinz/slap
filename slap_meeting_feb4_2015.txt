SLAP MEETING FEB 4

In Attendance:
	Jared, Grayson, Nicole, Nick, Zubir, Rod, Ofelia, Kara

Agenda:
	Icebreaker: If you could have any skill what skill would you have?
	PSLF Campaign
		Come up with fun name
		Update on Class Raps
			Input Petition Data
		Doodle for the Strategy Session
		Petitioning Days/Tabling
		Document info
	UnKoch My Campus followup
	Events//yo!
	Announcements


PSLF Campaign

	Fun Title Ideas  :
		Drop the Loan

	Class Raps :
		Nicole - Human Rights Policy 
		Good job!

	Petitioning Day
		Thursday Feb 5th  @ 2:00
			Nick, Nicole, Kara, Jared, Grayson, Ofelia
		Tuesday  Feb 10th @ 2:30 
			Rod, Nick, Nicole, Kara, Jared, Ofelia
		
		We suggest that after you complete a petition/class rap 
		to enter it into the spreadsheet linked in the working group
		and in the SLAP Google Drive.

UnKoch My Campus
	
	NO DRUGS

	Rod's team will be doing a research session sometime soon.

Events 

        Feb 5th  : 1:30-3:30
                   UFF collective bargaining session
                   CSB 221 UCF
        Feb 6th  : 4-7PM
                   JWJ Annual Meeting
                   CWA Hall
        Feb 7th  : YAYA Community Garden @ Fellsmere
		   /UNITE HERE Training
        Feb 11th : CLC Meeting
        Feb 15th : CIW Rally @ Wendys
                   2201 East Colonial Dr. Orlando, FL, 32803
	Feb 20th : Abortion Speakouts
		   6PM, room TBA on Working Group, will
		   probably be in the SU.

	LegCon!
		

Cubicle Hours

	Cubicle now has a sign in sheet so we can keep track of our hours.

Announcements
	
	Abortion Speakouts : Feb 20th at 6PM, room TBA on Working group

	Farmworker Petition on the SLAP @ UCF Public Page
