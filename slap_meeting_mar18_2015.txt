SLAP Meeting March 18

Agenda: 
	Debrief of Letter Drop
	Next Steps
	Open Forum Planning
	Announcements
	Phonebanking

In Attendance : Rod, Jose, Beth, Derrick*, Emalee, Zubir, Jared, Nicole, Nick, Ofelia, 

Debrief of Letter Drop
	
	Pros 
		- We got the number of Hitt's assistant
		- It's our first letter drop
		- Beth's response to rejection was awesome
		- The receptionist we spoke to is a student 
	Cons 
		- Started speaking before everyone entered the lobby
		- We gave the letter to the receptionist rather than Hitt's actual assistant

* Hitt's Assistant : Susan Laden, 407-823-1823

Next Steps:
	- Calling the secretary			Emalee, Rod, Jose, Ofelia - at the cubicle on Friday at 4
	- Bringing this up to the public forum 	(below)
	- A clear ask 				(below)	
	- Timeline 


Open Forum Planning 

	About the Open Forum:

		UCF students are invited to attend an open forum Monday, March 23, 2015, with 
		- President John C. Hitt
		- Dale Whittaker, provost and vice president (Academic Affairs)
		- Maribeth Ehasz, vice president (Student Development and Enrollment Services)
		- Dan Holsenbeck, vice president (University Relations) 
		- Bill Merck, Chief Financial Officer (Administration & Finance)

		The forum will be from 11:30 a.m. to 1 p.m. in the Pegasus Ballroom of the 
		Student Union. All students are invited to discuss their ideas, concerns and experiences.

	Who can go?
		Jose + Nick #2, Rod, Nick, Jared, Zubir, Emalee + Cody, 

	Question Brainstorm 
		- Have you heard about this program? 
		- Did you get the letter?
		- Did your assistant tell you about our call?
		- How do you think student debt affects students and faculty? 
		- Do you have any proposed solutions to fixing this problem?
			Follow up -> NOT WORKING! Why is X fact true if your solutions work 
		- Get debt info and use to question their plans?
		- Do you plan on pledging to this program?
			Why not?
				He needs information + meeting with us 
					when?
		- Provost : how would this affect the faculty you oversee?
		- Personal story of student who benefits from the program 
		- It looks bad on the university when students default on their loans; will you support this program to inform every student about the PSLF?
		- If you're so focused on jobs and academic freedom, why are professors afraid to talk about their concerns (should be more specific)
		- Will you take leadership and show that you care about faculty?

Question Script

	* Before asking, introduce yourself and state that you're a member of the Student Labor Action Project
	* Be direct, don't let him divert the question and don't lose your focus or get caught off guard. Remain calm and collected. 

	1) How do think student debt affects students, faculty, and staff on campus? 				- Nick
	2) Start off with some facts on student debt : What are you doing/What do you plan on doing about it?   - Jared 
	3) Well it obviously isn't working. (facts) Have you heard about the PSLF program?			- Zubir 
		  We sent you a letter and called your assistant about it, did you get it?
		  (Give Hitt and the rest of the board the letter + CFPB guidebook) 
		  (explain PSLF if necessary)
		  If you're not aware, the Consumer Financial Protection Bureau has a pledge ...
		  Hard ask : Will you pledge to notify students about this program (PSLF)?
	4) Did not give us a straight yes or no :								- Jose 
		What are your reservations? 
		Provost : Positive thing for 100 new faculty.
	5) Personal story 											- Emalee 

	A) If you're so focused on jobs and academic freedom, why are professors afraid to talk about their concerns (should be more specific) - Rod
 
	Nicole will write the pledge.
	Jared will print letters, pledge, CFPB guide 
	Nick will capture footage and photos the OF after he asks his question 


Announcements 

	Next week, instead of a meeting, we'll be going to a Unite Here rally. 
	We're leaving around 5 and we're gonna be carpooling there.
	
	Who wants to go? Jose, Rod, Nick, Emalee, Nicole, Jared, Ofelia, 
		Ofelia will post an updated address on the working group.

	CIW Concert this Saturday, no one can go :(
