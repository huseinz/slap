SLAP Meeting Oct 27 2015

In Attendance: Zubir, Nick, Chelsea, Nicole H, Matt, Jay, Jared, Ofelia, 

Agenda:

	VP Meeting Update - Email #5
	OSI Educational Forum
	Flyering
	Banner Making + Rebranding
	JWJ Conference

	Events:

		Fight for $15 March 
			Tuesday, Nov 10th at 5:30 PM. Lake Eola Park, 599 E Central Blvd, 32801

		CIW Publix Rally
			Saturday, Nov 21st at 10AM, 400 E Central Blvd, 32801
		
VP Meeting
	
	We'll be meeting with VP Rick Schell (and a few other people, probably Sheila Daniels from HR)
	on Friday, November 13th at 3PM in the President's office, Millican Hall, 3rd floor.

	We'll be planning for it in the coming weeks.

	In the meantime, we're recruiting people to show solidarity during the meeting (goal is at least 20)

	VOX - Nicole H: agreed, follow up with them near date
	KFB - Nicole M: followup with KFB VP
	JWJ - Zubir & Ofelia: followup with Jonathan
	UFF - Zubir: haven't met with Scott yet
	AFSCME - Nick: waiting on reply from Mike
	Dems - Cris: need confirmation

JWJ Conference

	Takes place Feb 12-14th in Washington DC, hasn't happened in like 4-5 years so it's a big deal.

	We want to secure SGA funding early, so we need to know who can go ASAP. If you commit to going, you
	have to go, SGA rules. Which is also why we're doing it early. 

OSI Educational Forum

	Still waiting on reply, the dude said he was busy with Homecoming. We should receive a reply by next week.

Flyering

	Matt & Chelsea - Neptune, completed
	Nicole H & Jay - Tower 4, busted
	Nick, Ofelia, Zubir - Libra, rescheduled to 11AM this Friday
	
	Note: we should look into seeing what channels are available for us to flyer legitimately...

Banner Making +

	Happening this Friday at 4PM, Nick's place (Riverwind) 

	Stuff to bring: sheet for banner, posterboards, paint, markers, etc.


